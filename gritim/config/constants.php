<?php

return [
    
    'admin_asset' => '/storage/admin/',
	'default_admin_photo' => '/storage/admin/profile/avatar1.png',

	// constant for api
	'error_status' => '0',
	'success_status' => '1',
	'form_validation' => '0',
	'error_default_msg' => 'Something went wrong!',
	'auth_error' => '404',
	'auth_error_msg' => 'User authentication failed!',
	'user_suspend_status' => '0',
	'user_suspend_msg' => 'Your account is suspended!',
    
];

