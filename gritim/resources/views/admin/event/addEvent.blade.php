@extends('admin.layouts.app')

@section('title')
{{ __($pageTitle) }}
@endsection

@section('style')
<style>
  #imgPreview {
    max-height: 150px;
    max-width: 150px;
    border-radius: 3px;
    margin: 5px;
  }

  input[type="file"] {
    display: none;
  }

  .custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
  }

  #nameReq:after, #emailReq:after, #passReq:after {
    content:" *";
    color: red;
  }

 
</style>
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">{{ __($pageTitle) }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('admin.home',app()->getLocale()) }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('events',app()->getLocale()) }}">Events</a></li>
            <li class="breadcrumb-item active">{{ __($pageTitle) }}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
   <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title"><?php echo $pageTitle; ?></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="addForm" methode="post">
              @csrf
              @if(@$event['id'] != "")
              <input type="hidden" class="form-control" name="id" id="id" value="{{ @$event['id'] }}">
              @endif
              <div class="card-body">
                
                <div class="form-group">
                    <label for="event_title" id="event_titleReq">Event Title</label>
                    <input type="text" class="form-control" name="event_title" id="event_title" placeholder="Enter Event Title" value="{{ @$event['event_title'] }}">
                    <p id="event_titleError" class="validation-error"></p>
                </div>
                <div class="row">
                  <div class="col-md-6">  
                    <div class="form-group">
                      <label for="description">Description</label>
                      <textarea class="form-control" name="description" id="description" placeholder="Enter Description">{{ @$event['description'] }}</textarea>
                      <p id="descriptionError" class="validation-error"></p>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="location" id="locationReq">Location</label>
                      <textarea class="form-control" name="location" id="location" placeholder="Enter Location">{{ @$event['location'] }}</textarea>
                      <p id="locationError" class="validation-error"></p>
                    </div>
                  </div>
                </div>
                  @php
                    $timestamp = time();
                    $iso8601 = date('c', $timestamp);
                    $iso8601 = substr($iso8601, 0, 16); 

                    if (@$event['start_time'] != NULL) {
                      $start_time = date('c', strtotime($event['start_time']));
                      $start_time = substr($start_time, 0, 16); 
                    } 
                    if (@$event['end_time'] != NULL) {
                      $end_time = date('c', strtotime($event['end_time']));
                      $end_time = substr($end_time, 0, 16); 
                    }  
                    
                  @endphp
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="start_time">Event Start</label>
                        <input type="datetime-local" class="form-control" name="start_time" id="start_time" value="{{ @$start_time }}" min="{{$iso8601}}">
                        <p id="start_timeError" class="validation-error"></p>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="end_time">Event End</label>
                        <input type="datetime-local" class="form-control" name="end_time" id="end_time" value="{{ @$end_time }}" min="{{$iso8601}}">
                        <p id="end_timeError" class="validation-error"></p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="is_free" id="is_freeReq">Is Free?</label>
                          <div class="form-group clearfix">
                            <div class="icheck-primary d-inline">
                              <input type="radio" id="radioPrimary2" name="is_free" value="1" @if(@$event['is_free'] == '1') checked @endif   onclick="disabledPrice()">
                              <label for="radioPrimary2">Yes
                              </label>
                            </div>
                            <div class="icheck-primary d-inline">
                              <input type="radio" id="radioPrimary1" name="is_free" value="0" @if(@$event['is_free'] == '0') checked  @endif    onclick="enabledPrice()">
                              <label for="radioPrimary1">No
                              </label>
                            </div>
                            
                          </div>
                          <p id="is_freeError" class="validation-error"></p>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="price" id="priceReq">Price</label>
                          <input type="text" class="form-control" name="price" id="price" placeholder="Enter price" value="{{ @$event['price'] }}">
                          <p id="priceError" class="validation-error"></p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="join_limit" id="join_limitReq">Join Limit</label>
                        <input type="number" class="form-control" name="join_limit" id="join_limit" placeholder="Enter Join Limit" value="{{ @$event['join_limit'] }}">
                        <p id="join_limitError" class="validation-error"></p>
                      </div>
                    </div>
                    <div class="col-md-6"> 
                      <div class="form-group">
                        <label for="user_id" id="user_idReq">Event Organiser</label>
                        <select class="form-control" name="user_id" id="user_id">
                          <option disabled selected hidden>Select Organiser</option>
                          @php
                          foreach ($user as $u) { @endphp
                          <option value="{{ $u->id }}" {{ ( @$event['user_id'] == $u->id ) ? 'selected' : '' }} >{{ $u->name }}</option>
                          @php   
                        }
                        @endphp
                        </select>
                        <p id="user_idError" class="validation-error"></p>
                      </div>
                    </div>
                  </div>
                  

                  @if(@$event['media'] == "")
                    @php 
                      $imagePath = url(config('constants.default_admin_photo')); 
                    @endphp
                  @else
                    @php 
                      $image = url('/public/images/events/'.$event['media']); 
                    @endphp
                    
                    @if(public_path('images/events/'.$event['media']))
                      @php 
                        $imagePath = $image; 
                      @endphp
                    @else
                      @php 
                        $imagePath = url(config('constants.default_admin_photo')); 
                      @endphp

                    @endif

                  @endif
                  <div class="form-group">
                    <input type="file" class="form-control" id="uploadImage" name="media">
                    <label for="uploadImage" class="custom-file-upload">
                      <i class="fa fa-upload"></i> Event Picture
                    </label>
                    <img id="imgPreview" src="{{ $imagePath }}">
                    <p id="mediaError" class="validation-error"></p>
                  </div>

              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" id="submitBtn"  class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.card -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    
    $(".nav-sidebar a").removeClass("active");
    $("#event").addClass('active');

  });

  function disabledPrice() {
    $("#price").attr('readonly','readonly').val('0.00');
  }

  function enabledPrice() {
    $("#price").removeAttr('readonly');
  } 

  var ids = $('.form-control').map(function() {
    return this.id;
  }).get();
  var countKey = Object.keys(ids).length;
  $.each(ids, function(key, value) {
    $("#"+value).on('change keyup paste mouseup input propertychange', function() {
      var items = $("#"+value). val();
      if(items != "")
      {
        $('#'+value+'Error').text('');

      }
    });
  });

  function readURL(input, idAttr) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $(idAttr).attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }

  function changeProfileImage(path, idAttr) {
    $(idAttr).attr('src', path);
  }


  $("#uploadImage").change(function() {
    readURL(this, '#imgPreview');
  });

  $("#addForm").on('submit',(function(e)
  {
    $.each(ids, function(key, value) {
      $('#'+value+'Error').text("");
    });

    e.preventDefault();
    let form = new FormData(this);
    let url = "";
    let user_id = "{{@$event['id']}}";
    if (user_id != "") {
      url = "{{ route('events.actionEdit',app()->getLocale()) }}";
    } else {
      url = "{{ route('events.actionAdd',app()->getLocale()) }}";
    }
    $.ajax({
      url: url,
      type: "POST",
      data: form,
      contentType: false,
      cache: false,
      processData:false,
      dataType:'JSON',
      beforeSend : function()
      {
        $("#err").fadeOut();
        $("#submitBtn").attr('disabled','disabled').html('<i class="fas fa-spinner fa-pulse mr-2"></i> Processing...');
      },
      success: function(data)
      {
          if (data.status == 1) {
            if (user_id == "") {
              $("#addForm")[0].reset();
              $("#imgPreview").attr('src','{{ $imagePath }}');
            }
            toastr.success(data.message);
          }
          else if (data.status == 2) {
            var countKey = Object.keys(data.error).length;
            $.each(data.error, function(key, value) {
              if(value != "")
              {
                $('#'+key+"Error").html(value);
              }
            });
            // $("#nameError").text(data.error.name);
            // $("#emailError").text(data.error.email);
            // $("#mobileError").text(data.error.mobile);
            // $("#passwordError").text(data.error.password);
          }
          else
          {
            toastr.error(data.message);
          }
      },
      error: function(e)
      {
        $("#err").html(e).fadeIn();
      },
      complete: function(e)
      {
        $("#submitBtn").removeAttr('disabled').html('Submit');
      }

    });

  }));
 
</script>
@endsection