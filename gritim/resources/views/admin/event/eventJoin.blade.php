@extends('admin.layouts.app')

@section('title')
{{ __($pageTitle) }}
@endsection


@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">{{ __($pageTitle) }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('admin.home',app()->getLocale()) }}">{{ __('Home') }}</a></li>
            <li class="breadcrumb-item active">{{ __($pageTitle) }}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        
        <!-- /.card -->

        <div class="card">
          <div class="card-header">
            <button type="button" id="del_btn" onClick="deleteData(1)" class="btn btn-danger float-left">Delete Selected</button>
            {{-- <a href="#" class="btn btn-primary float-right">Add Event</a> --}}
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="eventJoinTable" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th style="text-align:center;">
                  <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="far fa-square"></i>
                  </button>
                </th>
                <th>Title</th>
                <th>User</th>
                <th>Status</th>
                <th>Date</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
                  
              </tbody>
              <tfoot>
              <tr>
                <th style="text-align:center;">
                  <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="far fa-square"></i>
                  </button>
                </th>
                <th>Title</th>
                <th>User</th>
                <th>Status</th>
                <th>Date</th>
                <th>Action</th>
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    
    $(".nav-sidebar a").removeClass("active");
    $("#eventJoin").addClass('active');

    $('#eventJoinTable').DataTable({
      "pagingType": "full_numbers",
      "processing": true,
      "serverSide": true,
      "order": [0],
      "ajax": {
        "url": "{{ route('eventJoins.list',app()->getLocale()) }}",
        "dataType":"json",
        "type": "POST",
        "data":{"_token":"{{csrf_token()}}"}
      },
      "columnDefs": [{ 
        "targets": [0,5],
        "orderable": false
      }]
    });
  });

  function deleteData(btnType, id = null){
    let ids = [];
    if (btnType == 0) {
      ids.push(id);
    } else {
      $('.all_del:checkbox:checked').each(function(i){
        ids[i] = $(this).val();
      });
    }
    console.log(ids);
    if(ids.length === 0)
    {
      swal("Sorry!", "Please Select at least one record !", "error");
    }
    else
    {
      swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this records !",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $.ajax({
            url:'{{ route('eventJoins.delete',app()->getLocale()) }}',
            method:'POST',
            dataType:'JSON',
            data:{id:ids, "_token":"{{csrf_token()}}"},
            beforeSend : function()
            {
              if (btnType == 0) {
                $("#deleteBtn"+id).html('<i class="fas fa-spinner fa-pulse"></i>').addClass("disabled");
              } else {
                $("#del_btn").addClass("disabled").html('<i class="fas fa-spinner fa-pulse mr-2"></i> Processing...');
              }
            },
            success:function(data)
            {
                toastr.success(data.message);
                $('#eventJoinTable').DataTable().ajax.reload( null, false );
            },
            error: function()
            {
              toastr.error("Something went wrong!");
            },
            complete: function()
            {
              if (btnType == 0) {
                $("#deleteBtn"+id).html('<i class="fas fa-trash"></i>').removeClass("disabled");
              } else {
                $("#del_btn").removeClass("disabled").html('Delete Selected');
              }
            }
          });
        }   
      });
    }
  }
</script>
@endsection