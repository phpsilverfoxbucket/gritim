@extends('admin.layouts.app')

@section('title')
{{ __($pageTitle) }}
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">{{ __($pageTitle) }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('admin.home',app()->getLocale()) }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('events',app()->getLocale()) }}">Events</a></li>
            <li class="breadcrumb-item active">{{ __($pageTitle) }}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">

                @if($event['media'] == "")

                  @php 
                    $imagePath = url(config('constants.default_admin_photo')); 
                  @endphp

                @else
                  
                  @if(public_path('images/events/'.$event['media']))

                    @php 
                      $imagePath = url('public/images/events/'.$event['media']); 
                    @endphp

                  @else

                    @php 
                      $imagePath = url(config('constants.default_admin_photo')); 
                    @endphp

                  @endif

                @endif

                @if($event['status'] == "1")
                  @php
                    $status = '<i class="fas fa-spinner fa-pulse" id="statusUpdateLoader'.$event['id'].'" style="display:none"></i>&nbsp;<label class="form-switch"><input type="checkbox" checked onClick="changeStatus('.$event['id'].',0)" ><i></i></label>';
                  @endphp
                @else
                  @php
                    $status = '<i class="fas fa-spinner fa-pulse" id="statusUpdateLoader'.$event['id'].'" style="display:none"></i>&nbsp;<label class="form-switch"><input type="checkbox" onClick="changeStatus('.$event['id'].',1)"><i></i></label>';
                  @endphp
                @endif

                <img class="profile-user-img img-fluid img-circle" src="{{ $imagePath }}" id="profilePic" alt="User profile picture">
              </div>

              <h3 class="profile-username text-center">{{ $event['name'] }}</h3>
              <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                  <b>Active</b> <a class="float-right">{!! $status !!}</a>
                </li>
              </ul>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="card">
            <div class="card-header p-2">
              <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">Event Details</a></li>
                
              </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
              <div class="tab-content">
                <div class="tab-pane active" id="settings">
                  <div class="card-body">
                    <div class="form-group">
                      <label for="event_title">Event Title</label>
                      <input type="text" class="form-control" name="event_title" id="event_title" value="{{ $event['event_title'] }}" readonly>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="start_time">Event Start</label>
                          <input type="text" class="form-control" name="start_time" id="start_time" value="{{ $event['start_time'] }}" readonly>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="end_time">Event End</label>
                          <input type="text" class="form-control" id="end_time" name="end_time"  value="{{ $event['end_time'] }}" readonly>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="is_free">Is free?</label>
                          <input type="text" class="form-control" name="is_free" id="is_free" value="@php if(@$event['is_free'] == '0') { echo 'No'; } else { echo 'Yes'; } @endphp" readonly>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="price">Price</label>
                          <input type="text" class="form-control" id="price" name="price"  value="{{ $event['price'] }}" readonly>
                        </div>
                      </div>
                    </div>  
                        
                    <div class="form-group">
                      <label for="description">Description</label>
                      <textarea class="form-control" id="description" name="description"  readonly>{{ $event['description'] }}</textarea>
                    </div>
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div><!-- /.card-body -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    let flashMsg = "{{ Session::get('success') }}";
    if (flashMsg != "") {
      toastr.success(flashMsg);
    }
    $(".nav-sidebar a").removeClass("active");
    $("#event").addClass('active');
  });

  function changeStatus(id, value)
  {
    $.ajax({
        url: "{{ route('events.status',app()->getLocale()) }}",
        type: "POST",
        data: { id : id , value : value, "_token":"{{csrf_token()}}"},
        dataType:'JSON',
        beforeSend : function()
        {
          $("#statusUpdateLoader"+id).removeAttr('style');
        },
        success: function(data)
        {
            if (data.status == 1) {
              //toastr.success(data.message); 
              window.location.reload();
            }
            else
            {
              toastr.error(data.message);
            }
            
        },
        error: function()
        {
          toastr.error("Something went wrong!");
        },
        complete: function()
        {
          //$("#statusUpdateLoader"+id).attr('style','display:none;');
        }
    });
  }




</script>
@endsection