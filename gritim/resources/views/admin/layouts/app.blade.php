<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

	@include('includes.admin.head')

	<body class="hold-transition sidebar-mini layout-fixed">

	  	@include('includes.admin.header')
	    
	    @include('includes.admin.sidebar')

	    @yield('content')

	    @include('includes.admin.footer')
	    
	    @yield('script')

	</body>
</html>

