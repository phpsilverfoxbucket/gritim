@extends('admin.layouts.app')

@section('title')
{{ __($pageTitle) }}
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">{{ __($pageTitle) }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('admin.home',app()->getLocale()) }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('users',app()->getLocale()) }}">Users</a></li>
            <li class="breadcrumb-item active">{{ __($pageTitle) }}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">

                @if($user['image'] == "")

                  @php $imagePath = url(config('constants.default_admin_photo')); @endphp

                @else
                  
                  @if(public_path('images/user/'.$user['image']))

                    @php $imagePath = url('public/images/user/'.$user['image']); @endphp

                  @else

                    @php $imagePath = url(config('constants.default_admin_photo')); @endphp

                  @endif

                @endif

                @if($user['status'] == "1")
                  @php
                    $status = '<i class="fas fa-spinner fa-pulse" id="statusUpdateLoader'.$user['id'].'" style="display:none"></i>&nbsp;<label class="form-switch"><input type="checkbox" checked onClick="changeStatus('.$user['id'].',0)" ><i></i></label>';
                  @endphp
                @else
                  @php
                    $status = '<i class="fas fa-spinner fa-pulse" id="statusUpdateLoader'.$user['id'].'" style="display:none"></i>&nbsp;<label class="form-switch"><input type="checkbox" onClick="changeStatus('.$user['id'].',1)"><i></i></label>';
                  @endphp
                @endif

                <img class="profile-user-img img-fluid img-circle" src="{{ $imagePath }}" id="profilePic" alt="User profile picture">
              </div>

              <h3 class="profile-username text-center">{{ $user['name'] }}</h3>
              <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                  <b>Active</b> <a class="float-right">{!! $status !!}</a>
                </li>
              </ul>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="card">
            <div class="card-header p-2">
              <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">Profile</a></li>
                
              </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
              <div class="tab-content">
                <div class="tab-pane active" id="settings">
                  <div class="card-body">
                    <div class="form-group">
                      <label for="name">Name</label>
                      <input type="text" class="form-control" name="name" id="name" value="{{ $user['name'] }}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="title">Title</label>
                      <input type="text" class="form-control" name="title" id="title" value="{{ $user['title'] }}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="email">Email address</label>
                      <input type="email" class="form-control" id="email" name="email"  value="{{ $user['email'] }}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="mobile">Mobile</label>
                      <input type="text" class="form-control" id="mobile" name="mobile"  value="{{ $user['mobile'] }}" readonly>               
                    </div>
                    <div class="form-group">
                        <label for="bio">Bio</label>
                        <textarea class="form-control" name="bio" id="bio" placeholder="Enter Bio" readonly>{{ @$user['bio'] }}</textarea>
                    </div>
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div><!-- /.card-body -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    let flashMsg = "{{ Session::get('success') }}";
    if (flashMsg != "") {
      toastr.success(flashMsg);
    }
    $(".nav-sidebar a").removeClass("active");
    $("#user").addClass('active');
  });

  function changeStatus(id, value)
  {
    $.ajax({
        url: "{{ route('users.status',app()->getLocale()) }}",
        type: "POST",
        data: { id : id , value : value, "_token":"{{csrf_token()}}"},
        dataType:'JSON',
        beforeSend : function()
        {
          $("#statusUpdateLoader"+id).removeAttr('style');
        },
        success: function(data)
        {
            if (data.status == 1) {
              //toastr.success(data.message); 
              window.location.reload();
            }
            else
            {
              toastr.error(data.message);
            }
            
        },
        error: function()
        {
          toastr.error("Something went wrong!");
        },
        complete: function()
        {
          //$("#statusUpdateLoader"+id).attr('style','display:none;');
        }
    });
  }




</script>
@endsection