@extends('admin.layouts.app')

@section('title')
{{ __($pageTitle) }}
@endsection

@section('style')
<style>
  #imgPreview {
    max-height: 150px;
    max-width: 150px;
    border-radius: 3px;
    margin: 5px;
  }

  input[type="file"] {
    display: none;
  }

  .custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
  }

  #nameReq:after, #emailReq:after, #passReq:after {
    content:" *";
    color: red;
  }

 
</style>
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">{{ __($pageTitle) }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('admin.home',app()->getLocale()) }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('users',app()->getLocale()) }}">Users</a></li>
            <li class="breadcrumb-item active">{{ __($pageTitle) }}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
   <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title"><?php echo $pageTitle; ?></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="addForm" methode="post">
              @csrf
              @if(@$user['id'] != "")
              <input type="hidden" class="form-control" name="id" id="id" value="{{ @$user['id'] }}">
              @endif
              <div class="card-body">
                  <div class="form-group">
                      <label for="name" id="nameReq">Name</label>
                      <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" value="{{ @$user['name'] }}">
                      <p id="nameError" class="validation-error"></p>
                  </div>

                  <div class="form-group">
                      <label for="title">Title</label>
                      <input type="text" class="form-control" name="title" id="title" placeholder="Enter Title" value="{{ @$user['title'] }}">
                  </div>

                  <div class="form-group">
                      <label for="bio">Bio</label>
                      <textarea class="form-control" name="bio" id="bio" placeholder="Enter Bio">{{ @$user['bio'] }}</textarea>
                  </div>

                  <div class="form-group">
                      <label for="email" id="emailReq">Email</label>
                      <input type="email" class="form-control" name="email" id="email" placeholder="Enter Email" pattern="[a-z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-z0-9.-]+\.[a-z]{2,4}$" value="{{ @$user['email'] }}">
                      <p id="emailError" class="validation-error"></p>
                  </div>
                  
                  <div class="form-group">
                      <label for="mobile">Mobile Number</label>
                      <input type="number" class="form-control" name="mobile" id="mobile" placeholder="Enter Mobile number" value="{{ @$user['mobile'] }}">
                      <p id="mobileError" class="validation-error"></p>
                  </div>
                  <div class="form-group">
                      <label for="password" id="passReq">Password</label>
                      <input type="password" class="form-control" name="password" id="password" placeholder="Enter password">
                      <p id="passwordError" class="validation-error"></p>
                  </div>

                  @if(@$user['image'] == "")

                    @php $imagePath = url(config('constants.default_admin_photo')); @endphp

                  @else

                    @php $image = url('/public/images/user/'.$user['image']); @endphp
                    
                    @if(public_path('images/user/'.$user['image']))

                      @php $imagePath = $image; @endphp

                    @else

                      @php $imagePath = url(config('constants.default_admin_photo')); @endphp

                    @endif

                  @endif
                  <div class="form-group">
                    <input type="file" class="form-control" id="uploadImage" name="image">
                    <label for="uploadImage" class="custom-file-upload">
                      <i class="fa fa-upload"></i> Profile Picture
                    </label>
                    <img id="imgPreview" src="{{ $imagePath }}">
                    <p id="imageError" class="validation-error"></p>
                  </div>

              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" id="submitBtn"  class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.card -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    
    $(".nav-sidebar a").removeClass("active");
    $("#user").addClass('active');

  });

  function readURL(input, idAttr) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $(idAttr).attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }

  function changeProfileImage(path, idAttr) {
    $(idAttr).attr('src', path);
  }


  $("#uploadImage").change(function() {
    readURL(this, '#imgPreview');
  });

  $("#addForm").on('submit',(function(e)
  {
    $("#nameError").text('');
    $("#emailError").text('');
    $("#passwordError").text('');
    $("#mobileError").text('');
    e.preventDefault();
    let form = new FormData(this);
    let url = "";
    let user_id = "{{@$user['id']}}";
    if (user_id != "") {
      url = "{{ route('users.actionEdit',app()->getLocale()) }}";
    } else {
      url = "{{ route('users.actionAdd',app()->getLocale()) }}";
    }
    $.ajax({
      url: url,
      type: "POST",
      data: form,
      contentType: false,
      cache: false,
      processData:false,
      dataType:'JSON',
      beforeSend : function()
      {
        $("#err").fadeOut();
        $("#submitBtn").attr('disabled','disabled').html('<i class="fas fa-spinner fa-pulse mr-2"></i> Processing...');
      },
      success: function(data)
      {
          if (data.status == 1) {
            if (user_id == "") {
              $("#addForm")[0].reset();
              $("#imgPreview").attr('src','{{ $imagePath }}');
            }
            toastr.success(data.message);
          }
          else if (data.status == 2) {
            $("#nameError").text(data.error.name);
            $("#emailError").text(data.error.email);
            $("#mobileError").text(data.error.mobile);
            $("#passwordError").text(data.error.password);
          }
          else
          {
            toastr.error(data.message);
          }
      },
      error: function(e)
      {
        $("#err").html(e).fadeIn();
      },
      complete: function(e)
      {
        $("#submitBtn").removeAttr('disabled').html('Submit');
      }

    });

  }));
 
</script>
@endsection