@extends('admin.layouts.app')

@section('title')
{{ __($pageTitle) }}
@endsection

@section('style')
<style>
  #imgPreview {
    max-height: 150px;
    max-width: 150px;
    border-radius: 3px;
    margin: 5px;
  }

  input[type="file"] {
    display: none;
  }

  .custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
  }
</style>
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">{{ __($pageTitle) }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('admin.home',app()->getLocale()) }}">Home</a></li>
            <li class="breadcrumb-item active">{{ __($pageTitle) }}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">

                @if($adminData['image'] == "")

                  @php $imagePath = url(config('constants.default_admin_photo')); @endphp

                @else

                  @php $image = url('/storage/admin/profile/'.$adminData['image']); @endphp
                  
                  @if(storage_path('admin/profile/'.$adminData['image']))

                    @php $imagePath = $image; @endphp

                  @else

                    @php $imagePath = url(config('constants.default_admin_photo')); @endphp

                  @endif

                @endif

                <img class="profile-user-img img-fluid img-circle" src="{{ $imagePath }}" id="profilePic" alt="User profile picture">
              </div>

              <h3 class="profile-username text-center">{{ $adminData['name'] }}</h3>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="card">
            <div class="card-header p-2">
              <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">Profile</a></li>
                <li class="nav-item"><a class="nav-link" href="#changePass" data-toggle="tab">Change Password</a></li>
              </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
              <div class="tab-content">


                <div class="tab-pane active" id="settings">
                  <form id="updateProfileForm" methode="post">
                    @csrf
                    <div class="card-body">
                      <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="{{ $adminData['name'] }}">
                        <p id="nameError" class="validation-error"></p>
                      </div>
                      <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" value="{{ $adminData['email'] }}">
                        <p id="emailError" class="validation-error"></p>
                      </div>
                      <div class="form-group">
                        <input type="file" class="form-control" id="uploadImage" name="image">
                        <label for="uploadImage" class="custom-file-upload">
                          <i class="fa fa-upload"></i> Profile Picture
                        </label>
                        <img id="imgPreview" src="{{ $imagePath }}">
                        <p id="imageError" class="validation-error"></p>
                      </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                      <button type="submit" id="submitBtn" class="btn btn-primary">Submit</button>
                    </div>
                  </form>
                </div>
                <div class="tab-pane" id="changePass">
                  <form id="updatePasswordForm" methode="post">
                    @csrf
                    <div class="card-body">
                      <div class="form-group">
                        <label for="old_password">Current Password</label>
                        <input type="password" class="form-control" name="old_password" id="old_password" placeholder="Enter Current Password">
                        <p id="oldPasswordError" class="validation-error"></p>
                      </div>
                      <div class="form-group">
                        <label for="new_password">New Password</label>
                        <input type="password" class="form-control" name="new_password" id="new_password" placeholder="Enter New Password">
                        <p id="newPasswordError" class="validation-error"></p>
                      </div>
                      <div class="form-group">
                        <label for="confirm_password">Confirm Password</label>
                        <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Enter Confirm Password">
                        <p id="confirmPasswordError" class="validation-error"></p>
                      </div>

                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                      <button type="submit" id="submitBtnPass" class="btn btn-primary">Submit</button>
                    </div>
                  </form>
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div><!-- /.card-body -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    let flashMsg = "{{ Session::get('success') }}";
    if (flashMsg != "") {
      toastr.success(flashMsg);
    }
    $(".nav-sidebar a").removeClass("active");
    $("#profile").addClass('active');
  });

  function readURL(input, idAttr) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $(idAttr).attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }

  function changeProfileImage(path, idAttr) {
    $(idAttr).attr('src', path);
  }


  $("#uploadImage").change(function() {
    readURL(this, '#imgPreview');
  });

  $("#updateProfileForm").on('submit', (function(e) {
    $("#emailError").text('');
    $("#nameError").text('');
    $("#imageError").text('');

    e.preventDefault();
    let form = new FormData(this);
    $.ajax({
      url: "{{ route('profile.updateProfile',app()->getLocale()) }}",
      type: "POST",
      data: form,
      contentType: false,
      cache: false,
      processData: false,
      dataType: 'JSON',
      beforeSend: function() {
        $("#err").fadeOut();
        //$("#processingMsg").removeAttr('style');
        $("#submitBtn").attr('disabled', 'disabled').html('<i class="fas fa-spinner fa-pulse mr-2"></i> Processing...');
      },
      success: function(data) {
        if (data.status == 1) {
          //toastr.success(data.message);
          window.location.reload();
        } else {
          $("#emailError").text(data.email);
          $("#nameError").text(data.name);
          $("#imageError").text(data.image);

        }
      },
      error: function(e) {

        $("#err").html(e).fadeIn();
      },
      complete: function(e) {

        //$("#processingMsg").attr('style','display:none;');
        $("#submitBtn").removeAttr('disabled').html('Submit');
      }

    });

  }));

  $("#updatePasswordForm").on('submit', (function(e) {
    $("#oldPasswordError").text('');
    $("#newPasswordError").text('');
    $("#confirmPasswordError").text('');
    e.preventDefault();
    let form = new FormData(this);
    $.ajax({
      url: "{{ route('profile.changePassword',app()->getLocale()) }}",
      type: "POST",
      data: form,
      contentType: false,
      cache: false,
      processData: false,
      dataType: 'JSON',
      beforeSend: function() {
        $("#err").fadeOut();
        //$("#processingMsg").removeAttr('style');
        $("#submitBtnPass").attr('disabled', 'disabled').html('<i class="fas fa-spinner fa-pulse mr-2"></i> Processing...');
      },
      success: function(data) {
        console.log(data);
        if (data.status == 1) {
          $("#updatePasswordForm")[0].reset()
          toastr.success(data.message);
        } else {
          $("#oldPasswordError").text(data.old_password);
          $("#newPasswordError").text(data.new_password);
          $("#confirmPasswordError").text(data.confirm_password);
        }
      },
      error: function(e) {
        console.log(e);
        $("#err").html(e).fadeIn();
      },
      complete: function(e) {
        //$("#processingMsg").attr('style','display:none;');
        $("#submitBtnPass").removeAttr('disabled').html('Submit');
      }

    });

  }));
</script>
@endsection