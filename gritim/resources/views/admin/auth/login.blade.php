<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{ config('app.name', 'Admin') }} |  {{ __('Admin Log in') }}</title>
  <link rel="shortcut icon" type="image/x-icon" href="{{ url(config('constants.admin_asset')) }}/logo/gritim-logo.jpg">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{url('/storage/admin/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{url('/storage/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{url('/storage/admin/dist/css/adminlte.min.css')}}">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <b>{{ config('app.name', 'Admin') }}</b> {{ __('Admin') }}
  </div>
  <!-- /.login-logo -->
  <div class="alert alert-primary" role="alert" style="display:none;" id="authencateLogin">
        {{ __('Authenting...') }}
  </div>
  <div class="alert alert-danger" id="loginError"  style="display:none;">
  </div>
  <div class="alert alert-success" role="alert" style="display:none;" id="successLogin">
    {{ __('Login successfull. Redirecting...') }}
  </div>

  <div class="card" id="loginFormCard">
    <div class="card-body login-card-body">
      <p class="login-box-msg">{{ __('Sign in to start your session') }}</p>

      <form method="POST" id="loginForm">
        @csrf
        <div class="input-group mb-3">
          <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus pattern="[a-z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          @error('email')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          @error('password')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
        <div class="row">
          {{-- <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
              <label for="remember">
                {{ __('Remember Me') }}
              </label>
            </div>
          </div> --}}
          <!-- /.col -->
          {{-- <div class="col-4"> --}}
            <button type="submit" class="btn btn-primary btn-block">{{ __('Login') }}</button>
          {{-- </div> --}}
          <!-- /.col -->
        </div>
      </form>

      @if (Route::has('password.request'))
      <p class="mb-1">
        <a href="{{ route('password.request',app()->getLocale()) }}">{{ __('I forgot my password') }}</a>
      </p>
      @endif
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{url('/storage/admin/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{url('/storage/admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{url('/storage/admin/dist/js/adminlte.min.js')}}"></script>
<script type="text/javascript">

    $("#loginForm").on('submit',(function(e)
      {
        e.preventDefault();
        $('#loginError').attr('style','display: none');
        $('#loginError').html('');
        let form = new FormData(this);
        $.ajax({
            url: "{{ route('admin.login',app()->getLocale()) }}",
            type: "POST",
            data: form,
            contentType: false,
            cache: false,
            processData:false,
            dataType:'JSON',
            beforeSend : function()
            {
              $("#err").fadeOut();
              $("#authencateLogin").removeAttr('style');
              $("#loginBtn").attr('disabled','disabled');
              $("#loginFormCard").attr('style','display:none;');
            },
            success: function(data)
            {
              // console.log(data);
              if (data.status == 1) 
              {
                $('#successLogin').removeAttr('style');
                $("#loginFormCard").attr('style','display:none;');
                window.location = data.path;
              }
              else if (data.errors) {
                
                $('#loginError').removeAttr('style').html(data.message);
              }
              else
              {
                $('#loginError').removeAttr('style').html(data.message);
                $("#loginFormCard").removeAttr('style');
              }
            },
            error: function(e)
            {
              $("#loginFormCard").attr('style','display:block;');
              $("#authencateLogin").attr('style','display:none;');
              $('#loginError').removeAttr('style').html(e.responseJSON.errors['email']);
              $("#err").html(e).fadeIn();
            },
            complete: function(e)
            {
              $("#authencateLogin").attr('style','display:none;');
              $("#loginBtn").removeAttr('disabled');
            }

        });

      }));

</script>
</body>
</html>
