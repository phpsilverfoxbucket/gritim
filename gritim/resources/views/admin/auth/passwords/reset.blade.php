


<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{ config('app.name', 'Admin') }} |  {{ __('Reset Password') }}</title>
  <link rel="shortcut icon" type="image/x-icon" href="{{ url(config('constants.admin_asset')) }}/logo/gritim-logo.jpg">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{url('/storage/admin/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{url('/storage/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{url('/storage/admin/dist/css/adminlte.min.css')}}">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <b>{{ config('app.name', 'Admin') }}</b> {{ __('Reset Password') }}
  </div>
  <!-- /.login-logo -->
  <div class="alert alert-primary" role="alert" style="display:none;" id="authencateLogin">
        {{ __('Authenting...') }}
  </div>
  <div class="alert alert-danger" id="loginError"  style="display:none;">
  </div>
  <div class="alert alert-success" role="alert" style="display:none;" id="successLogin">
    {{ __('Password updated successfully. Redirecting...') }}
  </div>

  <div class="card" id="resetFormCard">
    <div class="card-body login-card-body">
      <p class="login-box-msg">{{ __('Reset Password') }}</p>

      <form method="POST" id="resetForm">
        @csrf
        
        <input type="hidden" name="token" value="{{ $token }}">

        <div class="input-group mb-3">
          <input type="password" id="password" class="form-control" name="password"  autocomplete="current-password" placeholder="Password" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          
          <span class="invalid-feedback" role="alert" style="display: none;" id="password_error"></span>
          
        </div>

        <div class="input-group mb-3">
          <input type="password" id="password_confirmation" class="form-control" name="password_confirmation"  autocomplete="confirm-password" placeholder="Confirm Password" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>

          <span class="invalid-feedback" role="alert" style="display: none;" id="confirm_error"></span>
        </div>

        <div class="row">
          
          <!-- /.col -->
            <button type="submit" class="btn btn-primary btn-block">{{ __('Reset Password') }}</button>
          
          <!-- /.col -->
        </div>
      </form>

     
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{url('/storage/admin/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{url('/storage/admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{url('/storage/admin/dist/js/adminlte.min.js')}}"></script>
<script type="text/javascript">

    $("#resetForm").on('submit',(function(e)
      {
        e.preventDefault();
        $('#loginError').attr('style','display: none');
        $('#loginError').html('');
        $('#password_error').attr('style','display: none');
        $('#confirm_error').attr('style','display: none');
        

        let form = new FormData(this);
        $.ajax({
            url: "{{ route('password.update',app()->getLocale()) }}",
            type: "POST",
            data: form,
            contentType: false,
            cache: false,
            processData:false,
            dataType:'JSON',
            beforeSend : function()
            {
              $("#err").fadeOut();
              $("#authencateLogin").removeAttr('style');
              $("#loginBtn").attr('disabled','disabled');
              $("#resetFormCard").attr('style','display:none;');
            },
            success: function(data)
            {
              // console.log(data);
              if (data.status == 1) 
              {
                $('#successLogin').removeAttr('style');
                $("#resetFormCard").attr('style','display:none;');
                window.location = data.path;
              }
              else if (data.errors) {
                
                $('#loginError').removeAttr('style').html(data.message);
              }
              else
              {
                $('#loginError').removeAttr('style').html(data.message);
                $("#resetFormCard").removeAttr('style');
              }
            },
            error: function(e)
            {
                console.log(e);

              $("#resetFormCard").attr('style','display:block;');
              $("#password_error").attr('style','display:block;').html(e.responseJSON.errors.password);
              $("#confirm_error").attr('style','display:block;').html(e.responseJSON.errors.password_confirmation);
              $("#authencateLogin").attr('style','display:none;');
              $("#err").html(e).fadeIn();
            },
            complete: function(e)
            {
              $("#authencateLogin").attr('style','display:none;');
              $("#loginBtn").removeAttr('disabled');
            }

        });

      }));

</script>
</body>
</html>
