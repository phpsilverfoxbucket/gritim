<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{ config('app.name', 'Admin') }} |  {{ __('Forgot Password') }}</title>
  <link rel="shortcut icon" type="image/x-icon" href="{{ url(config('constants.admin_asset')) }}/logo/gritim-logo.jpg">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{url('/storage/admin/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{url('/storage/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{url('/storage/admin/dist/css/adminlte.min.css')}}">
</head>

<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <b>{{ config('app.name', 'Admin') }}</b> {{ __('Reset Password') }}
  </div>
  <!-- /.login-logo -->
  <div class="alert alert-primary" role="alert" style="display:none;" id="fromProcess">
        {{ __('Sending...') }}
  </div>
  <div class="alert alert-danger" id="fromError"  style="display:none;">
  </div>
  <div class="alert alert-success" role="alert" style="display:none;" id="fromSuccess">
    {{ __('Check your Inbox.') }}
  </div>


  <div class="card" id="sendEmailFromCard">
    <div class="card-body login-card-body">
      <p class="login-box-msg">{{ __('Reset Password') }}</p>

      <form method="POST" id="sendEmailFrom">
        @csrf
        <div class="input-group mb-3">
          <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus pattern="[a-z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          @error('email')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
        
          <!-- /.col -->
          
        <button type="submit" id="submitBtn" class="btn btn-primary btn-block">{{ __('Send Password Reset Link') }}</button>
          
          <!-- /.col -->
        </div>
      </form>

    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{url('/storage/admin/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{url('/storage/admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{url('/storage/admin/dist/js/adminlte.min.js')}}"></script>
<script type="text/javascript">

    $("#sendEmailFrom").on('submit',(function(e)
      {
        e.preventDefault();
        $('#fromError').attr('style','display: none');
        $('#fromError').html('');
        $('#fromSuccess').attr('style','display: none');
        let form = new FormData(this);
        $.ajax({
            url: "{{ route('password.email',app()->getLocale()) }}",
            type: "POST",
            data: form,
            contentType: false,
            cache: false,
            processData:false,
            dataType:'JSON',
            beforeSend : function()
            {
              $("#err").fadeOut();
              $("#fromProcess").removeAttr('style');
              $("#loginBtn").attr('disabled','disabled');
              $("#sendEmailFromCard").attr('style','display:none;');
            },
            success: function(data)
            {
              // console.log(data);
              if (data.status == 1) 
              {
                $('#fromSuccess').removeAttr('style');
                $("#sendEmailFromCard").attr('style','display:block;');
                // window.location = data.path;
              }
              else if (data.errors) {
                
                $('#fromError').removeAttr('style').html(data.message);
              }
              else
              {
                $('#fromError').removeAttr('style').html(data.message);
                $("#sendEmailFromCard").removeAttr('style');
              }
            },
            error: function(e)
            {
              $("#sendEmailFromCard").attr('style','display:block;');
              $("#fromProcess").attr('style','display:none;');
              $('#fromError').removeAttr('style').html(e.responseJSON.errors['email']);
              $("#err").html(e).fadeIn();
            },
            complete: function(e)
            {
              $("#fromProcess").attr('style','display:none;');
              $("#loginBtn").removeAttr('disabled');
            }

        });

      }));

</script>
</body>
</html>