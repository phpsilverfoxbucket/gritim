<footer class="main-footer">
    <strong>Copyright &copy; {{ date('Y') }} {{ config('app.name', 'Admin') }}.</strong>
    All rights reserved.
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<script src="{{ url(config('constants.admin_asset')) }}/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ url(config('constants.admin_asset')) }}/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ url(config('constants.admin_asset')) }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="{{ url(config('constants.admin_asset')) }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ url(config('constants.admin_asset')) }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ url(config('constants.admin_asset')) }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ url(config('constants.admin_asset')) }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- daterangepicker -->
<script src="{{ url(config('constants.admin_asset')) }}/plugins/moment/moment.min.js"></script>
<script src="{{ url(config('constants.admin_asset')) }}/plugins/daterangepicker/daterangepicker.js"></script>
<script src="{{ url(config('constants.admin_asset')) }}/plugins/select2/js/select2.full.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ url(config('constants.admin_asset')) }}/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<script src="{{ url(config('constants.admin_asset')) }}/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="{{ url(config('constants.admin_asset')) }}/plugins/sweetalert/sweetalert.min.js"></script>
<script src="{{ url(config('constants.admin_asset')) }}/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>

<script src="{{ url(config('constants.admin_asset')) }}/plugins/datatable-buttons/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="{{ url(config('constants.admin_asset')) }}/plugins/datatable-buttons/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="{{ url(config('constants.admin_asset')) }}/plugins/datatable-buttons/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="{{ url(config('constants.admin_asset')) }}/plugins/datatable-buttons/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="{{ url(config('constants.admin_asset')) }}/plugins/datatable-buttons/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="{{ url(config('constants.admin_asset')) }}/plugins/datatable-buttons/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="{{ url(config('constants.admin_asset')) }}/plugins/datatable-buttons/buttons.colVis.min.js"></script>

<!-- AdminLTE App -->
<script src="{{ url(config('constants.admin_asset')) }}/dist/js/adminlte.js"></script>

<script src="{{ url(config('constants.admin_asset')) }}/plugins/toastr/toastr.min.js"></script>
<script>

  $('document').ready(function()
  {
    $('textarea').each(function(){
        $(this).val($(this).val().trim());
      }
    );
  });

  $('.select2').select2();
  
  $('.checkbox-toggle').click(function () {
    var clicks = $(this).data('clicks')
    if (clicks) {
      //Uncheck all checkboxes
      $('.all_del').prop('checked', false);
      $('.checkbox-toggle .far.fa-check-square').removeClass('fa-check-square').addClass('fa-square');
    } else {
      //Check all checkboxes
      $('.all_del').prop('checked', true);
      $('.checkbox-toggle .far.fa-square').removeClass('fa-square').addClass('fa-check-square');
    }
    $(this).data('clicks', !clicks)
  });

  
</script>