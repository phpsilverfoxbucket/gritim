<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Admin') }} - @yield('title')</title>


  <link rel="shortcut icon" type="image/x-icon" href="{{ url(config('constants.admin_asset')) }}/logo/gritim-logo.jpg">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ url(config('constants.admin_asset')) }}/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{ url(config('constants.admin_asset')) }}/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ url(config('constants.admin_asset')) }}/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url(config('constants.admin_asset')) }}/dist/css/adminlte.min.css">

  <link rel="stylesheet" href="{{ url(config('constants.admin_asset')) }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">

  <link rel="stylesheet" href="{{ url(config('constants.admin_asset')) }}/plugins/datatable-buttons/buttons.dataTables.min.css">




  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ url(config('constants.admin_asset')) }}/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ url(config('constants.admin_asset')) }}/plugins/daterangepicker/daterangepicker.css">

  <link rel="stylesheet" href="{{ url(config('constants.admin_asset')) }}/plugins/toastr/toastr.min.css">

  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{ url(config('constants.admin_asset')) }}/plugins/icheck-bootstrap/icheck-bootstrap.min.css">

  <link rel="stylesheet" href="{{ url(config('constants.admin_asset')) }}/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="{{ url(config('constants.admin_asset')) }}/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <style type="text/css">
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }

    /* Firefox */
    input[type=number] {
      -moz-appearance: textfield;
    }

    .disable-click {
      cursor: pointer;
      pointer-events: none;
      opacity: 0.5;
    }

    .validation-error {
      font-size: 14px;
      color: red;
    }

    .form-switch {
      display: inline-block;
      cursor: pointer;
      -webkit-tap-highlight-color: transparent;
    }

    .form-switch i {
      position: relative;
      display: inline-block;
      margin-right: .5rem;
      width: 46px;
      height: 26px;
      background-color: #e6e6e6;
      border-radius: 23px;
      vertical-align: text-bottom;
      transition: all 0.3s linear;
    }

    .form-switch i::before {
      content: "";
      position: absolute;
      left: 0;
      width: 42px;
      height: 22px;
      background-color: #C1C1C1;
      border-radius: 11px;
      transform: translate3d(2px, 2px, 0) scale3d(1, 1, 1);
      transition: all 0.25s linear;
    }

    .form-switch i::after {
      content: "";
      position: absolute;
      left: 0;
      width: 22px;
      height: 22px;
      background-color: #fff;
      border-radius: 11px;
      box-shadow: 0 2px 2px rgba(0, 0, 0, 0.24);
      transform: translate3d(2px, 2px, 0);
      transition: all 0.2s ease-in-out;
    }

    .form-switch:active i::after {
      width: 28px;
      transform: translate3d(2px, 2px, 0);
    }

    .form-switch:active input:checked+i::after {
      transform: translate3d(16px, 2px, 0);
    }

    .form-switch input {
      display: none;
    }

    .form-switch input:checked+i {
      background-color: #4BD763;
    }

    .form-switch input:checked+i::before {
      transform: translate3d(18px, 2px, 0) scale3d(0, 0, 0);
    }

    .form-switch input:checked+i::after {
      transform: translate3d(22px, 2px, 0);
    }
  </style>

  @yield('style')
</head>