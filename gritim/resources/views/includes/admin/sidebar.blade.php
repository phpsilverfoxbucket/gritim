<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <a href="{{ route('admin.home',app()->getLocale()) }}" class="brand-link">
    <img src="{{ url(config('constants.admin_asset')) }}/logo/gritim-logo.jpg" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
         style="opacity: .8">
    <span class="brand-text font-weight-light"><b>{{ config('app.name', 'Admin') }}</b></span>
  </a>


  <div class="sidebar">
   
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        @if(Auth::user()->image == "")

          @php $imagePath = url(config('constants.default_admin_photo')); @endphp

        @else

          @php $image = url('/storage/admin/profile/'.Auth::user()->image); @endphp
          
          @if(storage_path('admin/profile/'.Auth::user()->image))

            @php $imagePath = $image; @endphp

          @else

            @php $imagePath = url(config('constants.default_admin_photo')); @endphp

          @endif

        @endif
        {{-- <img src="{{ config('app.default_admin_photo', url('/storage/admin/profile/avatar1.png')) }}" class="img-circle elevation-2" id="sidebarProfileImg" alt="User Image"> --}}
        <img src="{{ $imagePath }}" class="img-circle elevation-2" id="sidebarProfileImg" alt="User Image">
        
      </div>
      <div class="info">
        <a href="{{ route('profile',app()->getLocale()) }}" class="d-block">{{ Auth::user()->name }}</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <li class="nav-header">General</li>
        <li class="nav-item">
          <a href="{{ route('admin.home',app()->getLocale()) }}" id="dashboard" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>Dashboard</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('users',app()->getLocale()) }}" id="user" class="nav-link">
            <i class="nav-icon fas fa-users"></i>
              <p>Users</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('events',app()->getLocale()) }}" id="event" class="nav-link">
            <i class="nav-icon fas fa-tasks"></i>
              <p>Events</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('eventJoins',app()->getLocale()) }}" id="eventJoin" class="nav-link">
            <i class="nav-icon fas fa-user-plus"></i>
              <p>Event Joiners</p>
          </a>
        </li>
        <li class="nav-header">Account Settings</li>
        <li class="nav-item">
          <a href="{{ route('profile',app()->getLocale()) }}" id="profile" class="nav-link">
            <i class="nav-icon fas fa-user-circle"></i>
              <p>Profile</p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>