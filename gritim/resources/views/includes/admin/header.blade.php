<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.home',app()->getLocale()) }}"><b>Home</b></a>
      </li>
    </ul>

    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-user-circle mr-2"></i> {{ Auth::user()->name }}
        </a>
        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
          <a href="{{ route('profile',app()->getLocale()) }}" class="dropdown-item">
            <i class="fas fa-pen mr-2"></i> Edit Profile
          </a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ route('admin.logout',app()->getLocale()) }}" onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">
            <i class="fas fa-power-off mr-2"></i> {{ __('Logout') }}
            <form id="logout-form" action="{{ route('admin.logout',app()->getLocale()) }}" method="POST" style="display: none;">
              @csrf
            </form>
          </a>
        </div>
      </li>
    </ul>
  </nav>