<!DOCTYPE html>
<html>

<head>
  <title>{{$title}}</title>
  <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700" rel="stylesheet">
</head>

<body>
  <div>
    <div style="background: #f7f7f7;  padding: 15px;  max-width: 590px;  margin: 0 auto;
    box-shadow: 1px 1px 1px 1px rgba(18, 18, 45,0.3);    margin-bottom: 15px;    margin-top: 15px;">
      <center>
        <div style="width:100%;max-width:590px;background:#f7f7f7;">
          <br>
          <center>

            <a href="#">
              <img alt="" src="https://staging.mixie.io/public/Adminassets/images/Log-in.png" width="170" height="auto" style="display:block;font-family:Helvetica,Arial,sans-serif;color:#666666;font-size:16px" border="0" class="CToWUd">
            </a>
          </center>
        </div>
        <br>

        <div style="width:100%;max-width:590px;background:#f7f7f7;height:auto;padding:10px 0 10px 0">
          <hr style="border:dashed 1px #e1e1e1;max-width:100%; box-shadow: 0px 0px 4px rgba(0, 0, 0,0.3);">

          <div style="width:100%;max-width:580px;background:#f7f7f7;height:auto;padding:0px 0 0px 0">
            <div style="font-family:'Lato',Helvetica,Arial,sans-serif;display:inline-block;margin:0px 0px 0 0;max-width:100%;width:100%;margin-right:0px">

              <div style="width:100%;max-width:580px;background:#f7f7f7;height:auto;padding:20px 0 0px 0">
                <div bgcolor="#f8f4e8" style="padding:0px 0% 0px 0%;font-size:22px;line-height:25px;font-family:'Lato',Helvetica,Arial,sans-serif;color:#fa7268;font-weight:700" class="m_-7788511936867687679padding-copy">Use OTP to rest your password
                </div>


                <div style="width:100%;max-width:580px;background:#f7f7f7;height:auto;padding:15px 0 0px 0">
                  <div bgcolor="#f8f4e8" align="center" style="padding:0px 0% 0px 0%;font-size:16px;line-height:25px;font-family:'Lato',Helvetica,Arial,sans-serif;color:#000000;font-weight:500" class="m_-7788511936867687679padding-copy">Hello {{$name}},<br/><br/>We have received a request to reset the password associated with your Mixie account.<br/>
                    <div>
                      {{-- <input type="hidden" value="{{$url}}" id="myInput"><br /> --}}
                      <b style="margin-bottom:5px;">OTP for reset Password</b>
                      <a href="#" style="display:block;color: #000; background: #f39c03;  padding:11px 60px; border-radius: 43px;  font-size: 20px;   margin: 15px 0; border: none;    color: #fff;">{{$otp}}</a>
                    </div>

                    You received this email because you requested a password reset. If you did not, please delete this mail.

                    <br/><br/>
                    The Admin Team
                  </div>

                  <!-- <div style="width:100%;max-width:580px;background:#f7f7f7;height:auto;padding:30px 0 15px 0">
                    <div style="width:100%;max-width:500px;background:#f7f7f7;height:auto;padding-bottom:0px">
                      <div align="center"><a href="https://www.mixie.io" style="font-size:14px; font-family: 'Roboto Slab', serif; font-weight:600;letter-spacing:0px;color:#f7f7f7;text-decoration:none; background-color:#fa7268; padding: 10px 19px;border-radius:3px;display:inline-block" class="m_-7788511936867687679mobile-button" target="_blank" data-saferedirecturl="https://www.mixie.io">Unsubscribe</a>
                      </div>
                    </div>
                  </div> -->


                  <div style="width:100%;max-width:580px;background:#f7f7f7;height:auto;padding:0px 0 10px 0">
                    <hr style="border:dashed 1px #e1e1e1;max-width:100%">
                  </div>

                  <!-- <p style="line-height:1;font-size:12px;margin:0 20px 10px 20px;padding:0 0 0 0;color:#777777;font-family:'Lato',Helvetica,Arial,sans-serif">
                    Follow us on social media!
                  </p>

                  <ul style="margin:0px; padding: 0px;">
                    <li style="display:inline-block; margin:0 5px;">
                      <a href="https://www.facebook.com/MixieHouston/" target="_blank"><img alt="" src="https://staging.mixie.io/public/Frontassets/images/icon/fb.png" /></a>
                    </li>
                    <li style="display:inline-block; margin:0 5px;">
                      <a href="https://www.instagram.com/mixieapp/" target="_blank"><img alt="" src="https://staging.mixie.io/public/Frontassets/images/icon/insta.png" /></a>
                    </li>
                    <li style="display:inline-block; margin:0 5px;">
                      <a href="https://youtu.be/6W6P25jyT6Y" target="_blank"><img alt="" src="https://staging.mixie.io/public/Frontassets/images/icon/youtab.png"></a>
                    </li>
                    <li style="display:inline-block; margin:0 5px;">
                      <a href="https://twitter.com/MixieHouston" target="_blank"><img alt="" src="https://staging.mixie.io/public/Frontassets/images/icon/twitter.png"></a>
                    </li>
                  </ul> -->

                  <div style="display:inline-block;text-align:center;color:#777777;margin:0px auto 26px auto;font-family:'Lato',Helvetica,Arial,sans-serif" align="center">

                    <div style="font-size:12px;color:#777777;margin:12px auto 30px auto;font-family:'Lato',Helvetica,Arial,sans-serif">
                      <p style="font-size: 11px"><b>Note:</b> Do not reply to this message,this message was auto-generated by the sender's security system.</p>


                      <p style="line-height:1;font-size:12px;margin:0 20px 30px 20px;padding:0 0 0 0;color:#777777;font-family:'Lato',Helvetica,Arial,sans-serif">
                        Mercova LLC, All rights reserved.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </center>
    </div>

  </div>
</body>

</html>