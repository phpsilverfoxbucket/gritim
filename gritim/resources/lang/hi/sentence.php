<?php 

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    "Login" => "लॉग इन करें",
    "E-Mail Address" => "ई-मेल एड्रेस",
    "Password" => "पासवर्ड",
    "Remember Me" => "मुझे याद रखना",
    "Forgot Your Password?" => "क्या आप पासवर्ड भूल गए?",
    "Sign In" => "लॉग इन करें",
    "I forgot my password" => "मैं अपना पासवर्ड भूल गया"

];