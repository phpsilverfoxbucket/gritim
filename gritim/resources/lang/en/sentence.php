<?php 

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    "Login" => "Login",
    "E-Mail Address" => "E-Mail Address",
    "Password" => "Password",
    "Remember Me" => "Remember Me",
    "Forgot Your Password?" => "Forgot Your Password?",
    "Sign In" => "Sign In",
    "I forgot my password" => "I forgot my password"

];