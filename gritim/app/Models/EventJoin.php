<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class EventJoin extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected  $table = 'event_join';

    protected $fillable = [
        'user_id', 'event_id', 'status', 'ip_address', 'created_at', 'updated_at'
    ];


}
