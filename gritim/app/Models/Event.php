<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Event extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected  $table = 'events';

    protected $fillable = [
        'event_title', 'start_time', 'end_time', 'location', 'description', 'media', 'event_rating', 'is_free', 'price', 'user_id', 'total_join', 'total_left', 'join_limit', 'status', 'created_at', 'updated_at', 'ip_address'
    ];


}
