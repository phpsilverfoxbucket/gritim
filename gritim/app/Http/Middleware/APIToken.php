<?php
namespace App\Http\Middleware;
use Closure;
use App\User;

class APIToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if($request->api_token){
        // $user = User::where('api_token',$request->api_token)->first();
        $user = User::where(['id'=>$request->user_id, 'api_token'=>$request->api_token])->first();

        if(!empty($user))
        {
          if($user->status == 0)
          {
            return response()->json(['status' => 0, 'message' =>config('constants.user_suspend_msg')]);
          }
          else
          {
            $request->merge(array('userData'=>$user));
            return $next($request);
          }
        }
        else
        {
          return response()->json(['status' => 404,'message' => config('constants.auth_error_msg')]);
        }   
      }
      
      return response()->json(['status' => 404,'message' => config('constants.auth_error_msg')]);
    }
}
