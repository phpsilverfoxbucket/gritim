<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            // return route('login');

            $guard = $request->segment(2);
            
            // return redirect(RouteServiceProvider::HOME);
            if($guard == "admin"){
                //user was authenticated with admin guard.
                return route('admin.login', ['locale' =>  'en']);
            } else {
                //default guard.
                return route('login', ['locale' =>  'en']);
            }
            
            
        }
    }
}

