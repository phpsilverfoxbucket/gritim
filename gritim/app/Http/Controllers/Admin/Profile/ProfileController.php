<?php

namespace App\Http\Controllers\Admin\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use File;
use Session;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adminData = Admin::where(['id'=>Auth::user()->id])->first();
        $pageTitle = "Update Profile";
        return view('admin.profile.index', compact('adminData', 'pageTitle'));
    }
   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {
        $rules = [
            'old_password' => 'required',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 200);
        }

        $adminData = Admin::where(['id'=>Auth::user()->id])->first();

        $match = Hash::check($request->old_password, $adminData['password']);

        if ($match === false) {
            return json_encode(array("status"=>"2", "old_password"=> "Current password does not match."));
        }
        
        $data['password'] = Hash::make($request->new_password);
        $updatePass = Admin::where(['id'=>Auth::user()->id])->update($data);

        if ($updatePass) {
            return json_encode(array("status"=>"1", "message"=> "Password changed successfully."));
        } else {
            return json_encode(array("status"=>"0", "message"=> "Something went wrong."));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:admins,email,'.Auth::user()->id,
            'image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 200);
        }

        $adminData = Admin::where(['id'=>Auth::user()->id])->first();

        if(!empty($request->file('image'))){
            $imageName = uniqid().'.'.$request->image->extension();  
               
            $request->image->move('storage/admin/profile/', $imageName);

            $image = $imageName;
        }

        $data['name'] = $request->name;
        $data['email'] = $request->email;
        if (isset($image)) {
            $image_path = storage_path("/admin/profile/".$adminData['image']);
            if(File::exists($image_path)) {
                File::delete($image_path);
            }
            $data['image'] = $image;
        }
        
        $update = Admin::where(['id'=>Auth::user()->id])->update($data);

        if ($update) {
            Session::flash('success', 'Profile updated successfully.');
            return json_encode(array("status"=>"1", "message"=> "Profile updated successfully."));

        } else {
            return json_encode(array("status"=>"0", "message"=> "Something went wrong."));
        }

    }

}
