<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = "Users";
        return view('admin.user.index', compact('pageTitle'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getlist(Request $request){
        $columns = array(
            0 =>  'id',
            1 =>  'image',
            2 =>  'name',
            3 =>  'email',
            4 =>  'mobile',
            5 =>  'status',
            6 =>  'created_at'
        );

        $totalData = User::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = ($request->input('order.0.dir') == "") ? "desc" : $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $posts = User::offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
            $totalFiltered = User::count();
        }else{
            $search = $request->input('search.value');
            $posts = User::where('name','like', "%{$search}%")
                        ->orWhere(function ($query)  use ($search) {
                $query->orWhere('email', 'like', "%{$search}%");
                $query->orWhere('mobile', 'like', "%{$search}%");
                $query->orWhere('created_at','like',"%{$search}%");
            })
            ->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir)
            ->get();

            $totalFiltered = User::where('name','like', "%{$search}%")
                         ->orWhere(function ($query)  use ($search) {
                $query->orWhere('email', 'like', "%{$search}%");
                $query->orWhere('mobile', 'like', "%{$search}%");
                $query->orWhere('created_at','like',"%{$search}%");
            })

            ->count();
        }

        $data = array();
        if($posts){
            foreach($posts as $r){
                $update_delete = '
                    <a href="'.route('users.show',[app()->getLocale(),$r->id]).'" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a><i class="mr-2"></i>
                    <a href="'.route('users.edit',[app()->getLocale(),$r->id]).'" class="btn btn-sm btn-primary"><i class="fa fa-pen"></i></a><i class="mr-2"></i>
                    <a id="deleteBtn'.$r->id.'" style="color:white;" onClick="deleteData(0,'.$r->id.')" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>';

                if($r->status == '1') 
                {
                    $status= '<label class="form-switch"><input type="checkbox" checked onClick="changeStatus('.$r->id.',0)" ><i></i></label>&nbsp;<i class="fas fa-spinner fa-pulse" id="statusUpdateLoader'.$r->id.'" style="display:none"></i>';
                } else { 
                    $status = '<label class="form-switch"><input type="checkbox" onClick="changeStatus('.$r->id.',1)"><i></i></label>&nbsp;<i class="fas fa-spinner fa-pulse" id="statusUpdateLoader'.$r->id.'" style="display:none"></i>';
                }

                if($r->image == '') 
                {
                    $imagePath = url(config('constants.default_admin_photo'));
                } else { 
                    $image = url('/public/images/user/'.$r->image);
                    if(public_path('images/user/'.$r->image))
                    {
                        $imagePath = $image;
                    } else {
                        $imagePath = url(config('constants.default_admin_photo')); 
                    }  
                }

                $imageHtml = '<img class="profile-user-img img-fluid img-circle" src="'.$imagePath.'" alt="User profile picture" style="height : 50px; width : 50px;">';

                $nestedData = array(); 
                $nestedData[] = '
                            <div class="icheck-primary" style="text-align:center;">
                                <input type="checkbox" value="'.$r->id.'" name="admin_id[]" class="all_del" id="chk_del_'.$r->id.'">
                                <label for="chk_del_'.$r->id.'"></label>
                            </div>';
                $nestedData[] = $imageHtml;
                $nestedData[] = $r->name;
                $nestedData[] = $r->email;
                $nestedData[] = $r->mobile;
                $nestedData[] = $status;
                $nestedData[] = date('d M Y',strtotime($r->created_at));
                $nestedData[] = $update_delete;
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"          => intval($request->input('draw')),
            "recordsTotal"  => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"          => $data
        );
        // echo json_encode($json_data);
        return response()->json($json_data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function actionStatus(Request $request)
    {
        $response = array();
        $response['status'] = "0"; 
        $id = $request->input('id');
        $value = $request->input('value');
        if($id != "" || $value != "")
        {
            $data = array(
                'status' => $value
            ); 

            $update = User::where(['id'=>$id])->update($data);

            if($update)
            {
                $response['status'] = "1";
                $response['message'] = "User status updated successfully.";
                Session::flash('success', 'User status updated successfully.');

            }   
            else
            {
                $response['message'] = "Something went wrong!";
            }   
        }
        else
        {
            $response['message'] = "Something went wrong!";
        }
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageTitle = "Add Users";
        return view('admin.user.addUser', compact('pageTitle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users,email',
            'mobile' => 'nullable|min:10|max:12|unique:users,mobile',
            'password' => 'required|min:6',
            'bio' => 'nullable',
            'image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // $validator->status = 2;
            $response = array("status"=>"2", "error" => $validator->errors());
            return response()->json($response, 200);
        }

        $data = $request->input();
        try{

            $user = new User;

            if(!empty($request->file('image'))){
                $imageName = uniqid().'.'.$request->image->extension();  
                   
                $request->image->move('public/images/user/', $imageName);

                $image = $imageName;
            }

            if (isset($image)) {
                // $image_path = public_path("/images/user/".$adminData['image']);
                // if(File::exists($image_path)) {
                //     File::delete($image_path);
                // }
                // $data['image'] = $image;
                $user->image = $image;

            }
            
            $user->name = $data['name'];
            $user->title = $data['title'];
            $user->email = $data['email'];
            $user->mobile = $data['mobile'];
            $user->bio = $data['bio'];
            $user->status = 1;
            $user->password = Hash::make($data['password']);
            $user->save();
            
            $response = array("status"=>"1", "message"=> "User added successfully.");
            return response()->json($response, 200);
        }
        catch(Exception $e){
            $response = array("status"=>"0", "message"=> "Something went wrong.");
            return response()->json($response, 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id)
    {
        $pageTitle = "User Details";
        if ($id == "") {
            return view('errors.404');
        } else {
            $user = User::where(['id'=>$id])->first();
            if (empty($user)) {
                return view('errors.404');
            }
        }
        return view('admin.user.viewUser', compact('pageTitle','user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $pageTitle = "Edit Users";
        if ($id == "") {
            return view('errors.404');
        } else {
            $user = User::where(['id'=>$id])->first();
            if (empty($user)) {
                return view('errors.404');
            }
        }
        return view('admin.user.addUser', compact('pageTitle','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users,email,'.$id,
            'mobile' => 'nullable|min:10|max:12|unique:users,mobile,'.$id,
            'password' => 'min:6|nullable',
            'bio' => 'nullable',
            'image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // $validator->status = 2;
            $response = array("status"=>"2", "error" => $validator->errors());
            return response()->json($response, 200);
        }

        $user = User::where(['id'=>$id])->first();
        if (empty($user)) {
            // $validator->status = 2;
            $response = array("status"=>"0", "message"=> "Something went wrong.");
            return response()->json($response, 200);
        }
        try{
            $data['name'] = $request->input('name');
            $data['title'] = $request->input('title');
            $data['email'] = $request->input('email');
            $data['mobile'] = $request->input('mobile');
            $data['bio'] = $request->input('bio');
            if ($request->input('password') != "") {
                $data['password'] = Hash::make($request->input('password'));
            }
            if(!empty($request->file('image'))){
                $imageName = uniqid().'.'.$request->image->extension();  
                   
                $request->image->move('public/images/user/', $imageName);

                $image = $imageName;
            }

            if (isset($image)) {
                $image_path = public_path("/images/user/".$user->image);
                if(File::exists($image_path)) {
                    File::delete($image_path);
                }
                $data['image'] = $image;
                

            }
            $update = User::where(['id'=>$id])->update($data);

            $response = array("status"=>"1", "message"=> "User updated successfully.");
            return response()->json($response, 200);
        }
        catch(Exception $e){
            $response = array("status"=>"0", "message"=> "Something went wrong.");
            return response()->json($response, 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $response = array();
        $response['status'] = "0"; 
        $ids = $request->input('id');

        foreach($ids as $id)
        {
            $user = User::where(['id'=>$id])->first();
            
            if(!empty($user))
            {
                $image_path = public_path("/images/user/".$user->image);
                if(File::exists($image_path)) {
                    File::delete($image_path);
                }
                $delete = User::where('id', $id)->delete();
            }     
        }

        $response['status'] = "1";
        $response['message'] = "User deleted successfully.";
        
        return response()->json($response, 200);
    }
}
