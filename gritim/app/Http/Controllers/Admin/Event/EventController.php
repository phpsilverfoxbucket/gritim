<?php

namespace App\Http\Controllers\Admin\Event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Event;
use File;
use Illuminate\Support\Facades\Validator;
use Session;

class EventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = 'Events';
        return view('admin.event.index',compact('pageTitle'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getlist(Request $request)
    {
        $columns = array(
            0 =>  'id',
            1 =>  'event_title',
            2 =>  'start_time',
            3 =>  'location',
            4 =>  'price',
            5 =>  'user_id',
            6 =>  'status',
            7 => 'created_at'
        );

        $totalData = Event::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = ($request->input('order.0.dir') == "") ? "desc" : $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $posts = Event::join('users', 'events.user_id', '=', 'users.id')
                ->select('events.*', 'users.name')
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
            $totalFiltered = Event::count();
        }else{
            $search = $request->input('search.value');
            $posts = Event::join('users', 'events.user_id', '=', 'users.id')
                ->select('events.*', 'users.name')
                ->where('event_title','like',"%{$search}%")
                ->orWhere('start_time','like',"%{$search}%")
                ->orWhere('location','like',"%{$search}%")
                ->orWhere('price','like',"%{$search}%")
                ->orWhere('users.name','like',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = Event::join('users', 'events.user_id', '=', 'users.id')
                ->select('events.*', 'users.name')
                ->where('event_title','like',"%{$search}%")
                ->orWhere('start_time','like',"%{$search}%")
                ->orWhere('location','like',"%{$search}%")
                ->orWhere('price','like',"%{$search}%")
                ->orWhere('users.name','like',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get()
                ->count();
        }

        $data = array();
        if($posts){
            foreach($posts as $r){
                $update_delete = '
                    <a href="'.route('events.show',[app()->getLocale(),$r->id]).'" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a><i class="mr-2"></i>
                    <a href="'.route('events.edit',[app()->getLocale(),$r->id]).'" class="btn btn-sm btn-primary"><i class="fa fa-pen"></i></a><i class="mr-2"></i>
                    <a id="deleteBtn'.$r->id.'" style="color:white;" onClick="deleteData(0,'.$r->id.')" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>';

                if($r->status == '1') 
                {
                    $status= '<label class="form-switch"><input type="checkbox" checked onClick="changeStatus('.$r->id.',0)" ><i></i></label>&nbsp;<i class="fas fa-spinner fa-pulse" id="statusUpdateLoader'.$r->id.'" style="display:none"></i>';
                } else { 
                    $status = '<label class="form-switch"><input type="checkbox" onClick="changeStatus('.$r->id.',1)"><i></i></label>&nbsp;<i class="fas fa-spinner fa-pulse" id="statusUpdateLoader'.$r->id.'" style="display:none"></i>';
                }

                $user = User::where(['id'=>$r->user_id])->first();

                $nestedData = array(); 
                $nestedData[] = '
                            <div class="icheck-primary" style="text-align:center;">
                                <input type="checkbox" value="'.$r->id.'" name="admin_id[]" class="all_del" id="chk_del_'.$r->id.'">
                                <label for="chk_del_'.$r->id.'"></label>
                            </div>';
                $nestedData[] = $r->event_title;
                $nestedData[] = date('d M Y h:i A',strtotime($r->start_time));
                $nestedData[] = $r->location;
                $nestedData[] = $r->price;
                $nestedData[] = $user->name;
                $nestedData[] = $status;
                $nestedData[] = date('d M Y',strtotime($r->created_at));
                $nestedData[] = $update_delete;
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"          => intval($request->input('draw')),
            "recordsTotal"  => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"          => $data
        );
        // echo json_encode($json_data);
        return response()->json($json_data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function actionStatus(Request $request)
    {
        $response = array();
        $response['status'] = "0"; 
        $id = $request->input('id');
        $value = $request->input('value');
        if($id != "" || $value != "")
        {
            $data = array(
                'status' => $value
            ); 

            $update = Event::where(['id'=>$id])->update($data);

            if($update)
            {
                $response['status'] = "1";
                $response['message'] = "Event status updated successfully.";
                Session::flash('success', 'Event status updated successfully.');

            }   
            else
            {
                $response['message'] = "Something went wrong!";
            }   
        }
        else
        {
            $response['message'] = "Something went wrong!";
        }
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageTitle = "Add Event";
        $user = User::select('id','name')->get();
        return view('admin.event.addEvent', compact('pageTitle','user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'event_title' => 'required|max:255',
            'start_time' => 'required',
            'end_time' => 'nullable',
            'location' => 'required',
            'description' => 'nullable',
            'is_free' => 'required',
            'price' => 'nullable|numeric',
            'media' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
            'user_id' => 'required',
            'join_limit' => 'nullable'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // $validator->status = 2;
            $response = array("status"=>"2", "error" => $validator->errors());
            return response()->json($response, 200);
        }

        $data = $request->input();
        try{

            $event = new Event;

            if(!empty($request->file('media'))){
                $imageName = uniqid().'.'.$request->media->extension();  
                   
                $request->media->move('public/images/events/', $imageName);

                $image = $imageName;
            }

            if (isset($image)) {
                // $image_path = public_path("/images/user/".$adminData['image']);
                // if(File::exists($image_path)) {
                //     File::delete($image_path);
                // }
                // $data['image'] = $image;
                $event->media = $image;

            }
            
            $event->event_title = $data['event_title'];
            $event->start_time = $data['start_time'];
            $event->end_time = $data['end_time'];
            $event->location = $data['location'];
            $event->description = $data['description'];
            $event->is_free = $data['is_free'];
            $event->price = $data['price'];
            $event->user_id = $data['user_id'];
            $event->join_limit = $data['join_limit'];
            $event->status = 1;
            $event->ip_address = request()->ip();
            $event->save();
            
            $response = array("status"=>"1", "message"=> "Event added successfully.");
            return response()->json($response, 200);
        }
        catch(Exception $e){
            $response = array("status"=>"0", "message"=> "Something went wrong.");
            return response()->json($response, 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id)
    {
        $pageTitle = "Event Details";
        $user = User::select('id','name')->get();
        if ($id == "") {
            return view('errors.404');
        } else {
            $event = Event::where(['id'=>$id])->first();
            if (empty($event)) {
                return view('errors.404');
            }
        }
        return view('admin.event.viewEvent', compact('pageTitle','event','user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $pageTitle = "Edit Event";
        $user = User::select('id','name')->get();
        if ($id == "") {
            return view('errors.404');
        } else {
            $event = Event::where(['id'=>$id])->first();
            if (empty($event)) {
                return view('errors.404');
            }
        }
        return view('admin.event.addEvent', compact('pageTitle','event','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');
        $rules = [
            'event_title' => 'required|max:255',
            'start_time' => 'required',
            'end_time' => 'nullable',
            'location' => 'required',
            'description' => 'nullable',
            'is_free' => 'required',
            'price' => 'nullable|numeric',
            'media' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
            'user_id' => 'required',
            'join_limit' => 'nullable'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // $validator->status = 2;
            $response = array("status"=>"2", "error" => $validator->errors());
            return response()->json($response, 200);
        }

        $event = Event::where(['id'=>$id])->first();
        if (empty($event)) {
            // $validator->status = 2;
            $response = array("status"=>"0", "message"=> "Something went wrong.");
            return response()->json($response, 200);
        }
        try{

            $data = array(
                
                'event_title' => $request->event_title,
                'start_time' => $request->start_time,
                'end_time' => $request->end_time,
                'location' => $request->location,
                'description' => $request->description,
                'is_free' => $request->is_free,
                'price' => $request->price,
                'user_id' => $request->user_id,
                'join_limit' => $request->join_limit,
                'ip_address' => request()->ip()
            );
            
            if(!empty($request->file('media'))){
                $imageName = uniqid().'.'.$request->media->extension();  
                   
                $request->media->move('public/images/events/', $imageName);

                $image = $imageName;
            }

            if (isset($image)) {
                $image_path = public_path("/images/events/".$event->media);
                if(File::exists($image_path)) {
                    File::delete($image_path);
                }
                $data['media'] = $image;
                

            }
            $update = Event::where(['id'=>$id])->update($data);

            $response = array("status"=>"1", "message"=> "Event updated successfully.");
            return response()->json($response, 200);
        }
        catch(Exception $e){
            $response = array("status"=>"0", "message"=> "Something went wrong.");
            return response()->json($response, 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $response = array();
        $response['status'] = "0"; 
        $ids = $request->input('id');

        foreach($ids as $id)
        {
            $user = Event::where(['id'=>$id])->first();
            
            if(!empty($user))
            {
                $image_path = public_path("/images/events/".$user->image);
                if(File::exists($image_path)) {
                    File::delete($image_path);
                }
                $delete = Event::where('id', $id)->delete();
            }     
        }

        $response['status'] = "1";
        $response['message'] = "Event deleted successfully.";
        
        return response()->json($response, 200);
    }
}
