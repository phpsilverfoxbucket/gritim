<?php

namespace App\Http\Controllers\Admin\Event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Event;
use App\Models\EventJoin;

class EventJoinController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = 'Event Joiner List';
        return view('admin.event.eventJoin',compact('pageTitle'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getlist(Request $request)
    {
        $columns = array(
            0 =>  'id',
            1 =>  'event_title',
            2 =>  'name',
            3 =>  'status',
            4 =>  'created_at'
        );

        $totalData = EventJoin::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = ($request->input('order.0.dir') == "") ? "desc" : $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $posts = EventJoin::join('users', 'event_join.user_id', '=', 'users.id')
	                    ->join('events', 'event_join.event_id', '=', 'events.id')
	                    ->select('event_join.*', 'users.name', 'events.event_title')
	                    ->offset($start)
			            ->limit($limit)
			            ->orderBy($order, $dir)
	                    ->get();

            $totalFiltered = EventJoin::count();
        }else{
            $search = $request->input('search.value');

            $posts = EventJoin::join('users', 'event_join.user_id', '=', 'users.id')
                        ->join('events', 'event_join.event_id', '=', 'events.id')
                        ->select('event_join.*', 'users.name', 'events.event_title')
                        ->where('events.event_title','like', "%{$search}%")
                        ->orWhere('users.name','like',"%{$search}%")
                        ->offset($start)
			            ->limit($limit)
			            ->orderBy($order, $dir)
                        ->get();

            $totalFiltered = EventJoin::join('users', 'event_join.user_id', '=', 'users.id')
	                    ->join('events', 'event_join.event_id', '=', 'events.id')
	                    ->select('event_join.*', 'users.name', 'events.event_title')
	                    ->where('events.event_title','like', "%{$search}%")
	                    ->orWhere('users.name','like',"%{$search}%")
	                    ->offset($start)
			            ->limit($limit)
			            ->orderBy($order, $dir)
	                    ->get()
            			->count();
        }

        $data = array();
        if($posts){
            foreach($posts as $r){
                $update_delete = '
                    <a id="deleteBtn'.$r->id.'" style="color:white;" onClick="deleteData(0,'.$r->id.')" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>';

                if($r->status == '1') 
                {
                    $status= '<label class="badge bg-success">Join</label>';
                } else { 
                    $status = '<label class="badge bg-danger">Left</label>';
                }

                $event = Event::where(['id'=>$r->event_id])->first();
                $user = User::where(['id'=>$r->user_id])->first();

                $nestedData = array(); 
                $nestedData[] = '
                            <div class="icheck-primary" style="text-align:center;">
                                <input type="checkbox" value="'.$r->id.'" name="admin_id[]" class="all_del" id="chk_del_'.$r->id.'">
                                <label for="chk_del_'.$r->id.'"></label>
                            </div>';
                $nestedData[] = $event->event_title;
                $nestedData[] = $user->name;
                $nestedData[] = $status;
                $nestedData[] = date('d M Y',strtotime($r->created_at));
                $nestedData[] = $update_delete;
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"          => intval($request->input('draw')),
            "recordsTotal"  => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"          => $data
        );
        // echo json_encode($json_data);
        return response()->json($json_data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $response = array();
        $response['status'] = "0"; 
        $ids = $request->input('id');

        foreach($ids as $id)
        {
            $user = EventJoin::where(['id'=>$id])->first();
            
            if(!empty($user))
            {
                
                $delete = EventJoin::where('id', $id)->delete();
            }     
        }

        $response['status'] = "1";
        $response['message'] = "Event Joiner deleted successfully.";
        
        return response()->json($response, 200);
    }
}
