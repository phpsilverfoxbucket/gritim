<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use App\Admin;
use App\Models\PasswordReset;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */


    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request, $locale = null, $token = null)
    {
        
        return view('admin.auth.passwords.reset')->with(
            ['token' => $token]
        );
    }

    public function reset(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'password' => 'required|min:6',
            'password_confirmation' => 'same:password',

        ]);

        
        $chekckToken = PasswordReset::where('token', $request->token)->first();

        if (empty($chekckToken)) {
            return json_encode(array("status"=>"0", "message"=> "Reset Password token expired."));
        } else {
            $data['password'] = Hash::make($request->password);
            $updatePass = Admin::where('email', $chekckToken['email'])->update($data);

            if ($updatePass) {
                PasswordReset::where('token', $request->token)->delete();

                $url = \URL::to('/') .'/'.app()->getLocale(). '/admin/login';
                return json_encode(array("status"=>"1", "message"=> "Password reset successfully.", "path" => $url));
            } else {
                return json_encode(array("status"=>"0", "message"=> "Something went wrong."));
            }
            
        }
    }
}
