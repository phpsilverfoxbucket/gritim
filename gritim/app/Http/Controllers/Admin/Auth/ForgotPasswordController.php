<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Admin;
use Auth;
use Mail;

use Illuminate\Http\JsonResponse;

use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;
use App\Models\PasswordReset;



class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    // use SendsPasswordResetEmails;

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\View\View
     */
    public function showLinkRequestForm()
    {   
        return view('admin.auth.passwords.email');
    }

    /**
     * Forget Password
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
        ]);

        $chekckEMail = Admin::where('email', $request['email'])->first();

        if (empty($chekckEMail)) {
           
            return json_encode(array("status"=>"0", "message"=> "Email address dose not exist."));
        }

        // generate token

        $token = bin2hex(openssl_random_pseudo_bytes(32));

        $checkResetEmail = PasswordReset::where('email', $request['email'])->first();
       

        if (empty($checkResetEmail)) {
            $data = $request->input();
            try{
                $resetToken = new PasswordReset;
                
                $resetToken->token = $token;
                $resetToken->email = $data['email'];
                $resetToken->save();
                
                // return json_encode(array("status"=>"0", "message"=> "Insert successfully."));
            }
            catch(Exception $e){
                
                return json_encode(array("status"=>"0", "message"=> "operation failed."));
            }
           
        } else {
            $resetData = array('token' => $token);
            try{
                $action = PasswordReset::where('email', $request['email'])->update($resetData);
                // return json_encode(array("status"=>"0", "message"=> "Updated successfully."));
            }
            catch(Exception $e){
                return json_encode(array("status"=>"0", "message"=> "operation failed."));
            }
        }


        
        $name = $chekckEMail['name'];

        $title = "Reset Your Password.!";

        $email = $request['email'];
        $url = \URL::to('/') .'/'.app()->getLocale(). '/admin/password/reset/' . $token;

        $data = array('title' => $title, 'url' => $url, 'email' => $email, 'name' => $name);

        try{
            Mail::send('emails.reset', $data, function ($message) use ($data) {
                $message->from('no-reply@admin.io', "Admin.io")->subject($data['title']);
                $message->to($data['email']);
            });
        }  catch (\Swift_TransportException $e) {
            \Log::debug($e);
            return json_encode(array("status"=>"0", "message"=> "operation failed.", "error" => $e));
        }


        return json_encode(array("status"=>"1", "message"=> "Check your Inbox."));

    }

    


}
