<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Controllers\API\CommonModal;
use App\User;
use DB;
use Validator;
use Illuminate\Support\Facades\Hash;
use Mail;

class AuthController extends BaseController
{
	
	private $apiToken;
	public function __construct()
	{
		$this->apiToken = bin2hex(openssl_random_pseudo_bytes(32));
	}

	public function register(Request $request)
	{
		$rules = [
			'name' => 'required',
			'email' => 'required|email|unique:users,email',
			'mobile' => 'required|min:10|max:12|unique:users,mobile',
			'password' => 'required|min:6',
		];

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) 
		{
			$errorString = implode(",",$validator->messages()->all());
			return $this->sendError($errorString,'');
		}
		else
		{
			try
			{
				$postArray = [
					'name' => ucfirst($request->name),
					'email' => $request->email,
					'mobile' => $request->mobile,
					'password' => Hash::make($request->password),
					'api_token' => $this->apiToken,
					'status' => 1,
					'device_type' => $request->device_type,
					'device_token' => $request->device_token,
				];

				$user = User::create($postArray);
				$user_id = $user->id;

				if ($user_id) {
					$user_details = CommonModal::user_details($user_id);
					$response = array(
						'user_id' => $user->id,
						'api_token' => $this->apiToken,
						'user_data' => $user_details
					);
					return $this->sendResponse('Registration successfully.',$response);
				} else {
					return $this->sendError('Registration failed, please try again.');
				}
			}
			catch(Exception $e){
				return $this->sendSpeError();
			}
		}
	}

	public function login(Request $request)
	{
		$rules = [
			'email'=>'required|email',
			'password'=>'required|min:6',
			'device_type'=>'required',
			'device_token'=>'required'
		];
		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails())
		{
			$errorString = implode(",",$validator->messages()->all());
			return $this->sendError($errorString,'');
		} 
		else 
		{
			$user = User::where('email',$request->email)->first();
			if($user) 
			{
				if($user->status == '1')
				{
					if(password_verify($request->password, $user->password)) 
					{
						$postArray = [
							'api_token' => $this->apiToken,
							'device_type'=> $request->device_type,
							'device_token'=> $request->device_token
						];

						$login = User::where('id',$user->id)->update($postArray);
						if($login) 
						{
							$user_details = CommonModal::user_details($user->id);
							$response = array(
								'user_id' => $user->id,
								'api_token' => $this->apiToken,
								'user_data' => $user_details
							);
							
							return $this->sendResponse('You are logged in successfully.',$response);
						}
						else
						{
							return $this->sendSpeError();
						}
					} 
					else 
					{
						return $this->sendError('The password that you have entered is incorrect.');
					}
				}
				else
				{
					return $this->sendSuspendError();
				}
			} 
			else 
			{
				try
				{
					$postArray = [
						'name' => '',
						'email' => $request->email,
						'mobile' => '',
						'password' => Hash::make($request->password),
						'api_token' => $this->apiToken,
						'status' => 1,
						'device_type' => $request->device_type,
						'device_token' => $request->device_token,
					];

					$user = User::create($postArray);
					$user_id = $user->id;

					if ($user_id) {
						$user_details = CommonModal::user_details($user_id);
						$response = array(
							'user_id' => $user->id,
							'api_token' => $this->apiToken,
							'user_data' => $user_details
						);
						return $this->sendResponse('Registration successfully..',$response);
					} else {
						return $this->sendError('Registration failed, please try again.');
					}
				}
				catch(Exception $e){
					return $this->sendSpeError();
				}
			}
		}
	}

	public function logout(Request $request)
	{
		$id = $request->user_id;
		$token = $request->api_token;
		$user = User::where('api_token',$token)->where('id',$id)->first();
		if($user) {
			$postArray = ['api_token' => null];
			$logout = User::where('id',$user->id)->update($postArray);
			if($logout) {
				return $this->sendResponse('You are logged out successfully.');
			}
		} else {
			return $this->sendError('Invalid credentials.');
		}
	}

	public function forgotPassword(Request $request)
	{
		$rules = [
			'email'=>'required|email'
		];

		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails())
		{
			$errorString = implode(",",$validator->messages()->all());
			return $this->sendError($errorString,'');
		} 
		else 
		{
			$user = User::where('email',$request->email)->first();
			if($user) 
			{
				
				$otp = rand(1000,9999);
				$postArray = ['otp' => $otp];
				$logout = User::where('id',$user->id)->update($postArray);

				$title = "Reset Your Password.!";
				$email = $request->email;
				$name = $user->name;

				$data = array('title' => $title, 'otp' => $otp, 'email' => $email, 'name' => $name);

				try{
					Mail::send('emails.otpLink', $data, function ($message) use ($data) {
						$message->from('no-reply@admin.io', "Admin.io")->subject($data['title']);
						$message->to($data['email']);
					});
					return $this->sendResponse('Your e-mail verification OTP sent to '.$email.' Please verifiy your mail to login.');
				}  catch (\Swift_TransportException $e) {
					\Log::debug($e);
					return $this->sendSpeError();
				}
			}
			else
			{
				return $this->sendError('Invalid credentials.');
			}
		}

	}

	public function verifyOtp(Request $request)
	{
		$rules = [
			'email'=>'required|email',
			'otp' => 'required|digits:4'
		];

		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails())
		{
			$errorString = implode(",",$validator->messages()->all());
			return $this->sendError($errorString,'');
		} 
		else 
		{
			$user = User::where('email',$request->email)->first();
			if($user) 
			{
				if ($user->status == 1) {
					if ($user->otp == $request->otp) 
					{
						$postArray = ['otp' => null];
						$updateOtp = User::where('id',$user->id)->update($postArray);
						if ($updateOtp) {
							return $this->sendResponse('Reset your password for Login.');
						} else {
							return $this->sendSpeError();
						}
					} 
					else 
					{
						return $this->sendError('Invalid OTP.');
					}
				} else {
					return $this->sendSuspendError();
				}
			}
			else
			{
				return $this->sendError('Invalid credentials.');
			}
		}
	}

	public function resetPassword(Request $request)
	{
		$rules = [
			'email'=>'required|email',
			'password'=>'required|min:6',
			'confirm_password' => 'same:password'
		];

		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails())
		{
			$errorString = implode(",",$validator->messages()->all());
			return $this->sendError($errorString,'');
		} 
		else 
		{
			$user = User::where('email',$request->email)->first();
			
			if(empty($user)) 
			{
				return $this->sendError('Invalid credentials!');
			}
			elseif ($user->status == '0') 
			{
				return $this->sendSuspendError();
			}
			else
			{	
				$postArray = ['password' => Hash::make($request->password)];
				$update = User::where('id',$user->id)->update($postArray);
				if ($update) {
					return $this->sendResponse('Password reset successfully. Please Login now.');
				} else {
					return $this->sendSpeError();
				}
			}
		}
	}


}