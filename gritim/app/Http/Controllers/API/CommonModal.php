<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\User;
use App\Models\Event;
use App\Models\EventJoin;
use Validator;
use File;

class CommonModal
{
    // \DB::enableQueryLog();
    // $user = Event::orderBy('id','DESC')->skip($num)->take(10)->get();
    // $query = \DB::getQueryLog();
    // print_r($query);

    public static function user_details($id)
    {
        $user = User::where('id', $id)->first();
        $data = array();
        if (!empty($user)) {
            if ($user->image == NULL) {
                $image = '';
            } else {
                $image_path = public_path("/images/user/" . $user->image);
                if (File::exists($image_path)) {
                    $image = url("public/images/user/" . $user->image);
                } else {
                    $image = '';
                }
            }

            $data['id'] = $user->id;
            $data['name'] = $user->name;
            $data['email'] = $user->email;
            $data['mobile'] = $user->mobile;
            $data['bio'] = (string)$user->bio;
            $data['rating'] = (string)$user->rating;
            $data['image'] = $image;
            $data['status'] = (string)$user->status;
            $data['created_at'] = date('d-m-Y h:i:s', strtotime($user->created_at));
        }
        return $data;
    }

    public static function event_list($num, $user_id)
    {
        $num = $num * 10;

        $events = Event::orderBy('id', 'DESC')->skip($num)->take(10)->get();

        $jsonArray = array();

        if (count($events) > 0) {
            foreach ($events as $row) {
                if ($row->media == NULL) {
                    $image = '';
                } else {
                    $image_path = public_path("/images/events/" . $row->media);
                    if (File::exists($image_path)) {
                        $image = url("public/images/events/" . $row->media);
                    } else {
                        $image = '';
                    }
                }
                $data['id'] = $row->id;
                $data['event_title'] = $row->event_title;
                $data['start_time'] = $row->start_time;
                $data['end_time'] = $row->end_time;
                $data['location'] = $row->location;
                $data['description'] = $row->description;
                $data['media'] = $image;
                $data['event_rating'] = (string)$row->event_rating;
                $data['is_free'] = (string)$row->is_free;
                $data['price'] = $row->price;
                $data['total_join'] = (string)$row->total_join;
                $data['total_left'] = (string)$row->total_left;
                $data['join_limit'] = (string)$row->join_limit;
                $data['status'] = (string)$row->status;
                $data['created_at'] = date('d-m-Y h:i:s', strtotime($row->created_at));
                $data['user_data'] = self::user_details($row->user_id);
                $data['join_status'] = (string)self::getJoinStatus($row->id, $user_id);
                $jsonArray[] = $data;
            }
        }
        return $jsonArray;
    }
    public static function event_detail($event_id, $user_id)
    {
        $event = Event::where(['id' => $event_id])->first();
        $data = array();
        if (!empty($event)) {
            if ($event->media == NULL) {
                $image = '';
            } else {
                $image_path = public_path("/images/events/" . $event->media);
                if (File::exists($image_path)) {
                    $image = url("public/images/events/" . $event->media);
                } else {
                    $image = '';
                }
            }

            $data['id'] = $event->id;
            $data['event_title'] = $event->event_title;
            $data['start_time'] = $event->start_time;
            $data['end_time'] = $event->end_time;
            $data['location'] = $event->location;
            $data['description'] = $event->description;
            $data['media'] = $image;
            $data['event_rating'] = (string)$event->event_rating;
            $data['is_free'] = (string)$event->is_free;
            $data['price'] = $event->price;
            $data['join_left'] = self::getEventJoinCount($event->id, $event->join_limit);
            //$data['total_left'] = (string)$event->total_left;
            $data['join_limit'] = (string)$event->join_limit;
            $data['status'] = (string)$event->status;
            $data['created_at'] = date('d-m-Y h:i:s', strtotime($event->created_at));
            $data['user_data'] = self::user_details($event->user_id);
            $data['join_status'] = (string)self::getJoinStatus($event->id, $user_id);
        }
        return $data;
    }


    public static function getJoinStatus($event_id, $user_id)
    {
        $joinStatus = EventJoin::where(['event_id' => $event_id, 'user_id' => $user_id])->first();

        if (!empty($joinStatus)) {
            // $data['join_status'] = $joinStatus->status;
            return $joinStatus->status;
        }
        return 0;
    }

    public static function getEventJoinCount($event_id, $limit)
    {
        $data = array();
        $joinCount = EventJoin::where(['event_id' => $event_id, 'status' => 1])->count();
        
            if (!empty($joinCount)) {
                $data['left'] = $limit - $joinCount;
            } else {
                $data['left'] = $limit;
            }
            $data['join'] = $joinCount;

        return $data;
    }
}
