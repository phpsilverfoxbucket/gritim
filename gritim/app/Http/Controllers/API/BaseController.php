<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;

class BaseController extends Controller
{
    
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($message, $result = [])
    {
    	$response = [
            'status' => config('constants.success_status'),
            'message' => $message
        ];
        if (!empty($result)) {
            $response = array_merge($response,$result);
        }
        return response()->json($response, 200);
    }

    public function sendResponsePagination($result, $message,$offset)
    {
        $response = [
            'status' => config('constants.success_status'),
            'offset' => $offset,
            'message' => $message,
            'data'    => $result,
        ];

        return response()->json($response, 200);
    }


    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 200)
    {
    	$response = [
            'status' => config('constants.error_status'),
            'message' => $error,
        ];

        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }
        return response()->json($response, $code);
    }

    public function sendSpeError()
    {
        $response = [
            'status' => config('constants.error_status'),
            'message' => config('constants.error_default_msg'),
        ];

        return response()->json($response, 200);
    }

    public function sendSuspendError()
    {
        $response = [
            'status' => config('constants.user_suspend_status'),
            'message' => config('constants.user_suspend_msg'),
        ];

        return response()->json($response, 200);
    }

}