<?php

namespace App\Http\Controllers\API\User;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Controllers\API\CommonModal;
use App\User;
use Validator;
use Illuminate\Support\Facades\Hash;
use File;

class UserController extends BaseController
{
	
	public function changePassword(Request $request)
	{
		$rules = [
			'old_password'=>'required',
			'password'=>'required|min:6',
			'confirm_password' => 'same:password',
			'user_id' => 'required',
			'api_token' => 'required',
		];

		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails())
		{
		  $errorString = implode(",",$validator->messages()->all());
		  return $this->sendError($errorString,'');
		} 
		else 
		{
			$user = $request->userData;
			if (empty($user)) {
				return $this->sendSpeError();
			}

	  		$postArray = ['password' => Hash::make($request->password)];
		  	$update = User::where('id',$user->id)->update($postArray);
	  		if ($update) {
	  			return $this->sendResponse('Password changed successfully. Please Login now.');
	  		} else {
	  			return $this->sendSpeError();
	  		}
		}
	}

	public function updateProfile(Request $request)
	{
		$rules = [
		    'name' => 'required',
		    'email' => 'required|email|unique:users,email,'.$request->user_id,
		    'mobile' => 'required|min:10|max:12|unique:users,mobile,'.$request->user_id,
		    'image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
		];

		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails())
		{
		  $errorString = implode(",",$validator->messages()->all());
		  return $this->sendError($errorString,'');
		} 
		else 
		{
			$user = $request->userData;
			if (empty($user)) {
				return $this->sendSpeError();
			}

			if($request->hasfile('image')){
			    $imageName = uniqid().'.'.$request->image->extension();  
			    $request->image->move('public/images/user/', $imageName);
			    $image = $imageName;
			}

			$data['name'] = $request->name;
			$data['email'] = $request->email;
			$data['mobile'] = $request->mobile;

			if (isset($image)) {
			    $image_path = public_path("/images/user/".$user->image);
			    if(File::exists($image_path)) {
			        File::delete($image_path);
			    }
			    $data['image'] = $image;
			}
	  		
		  	$update = User::where('id',$user->id)->update($data);
	  		if ($update) {

	  			$user_details = CommonModal::user_details($user->id);
	  			$response = array(
	  				'user_id' => $user->id,
	  				'api_token' => $user->api_token,
	  				'user_data' => $user_details
	  			);
	  			return $this->sendResponse('Your profile updated successfully.',$response);
	  		} else {
	  			return $this->sendSpeError();
	  		}
		}
	}

	// public function uploadMultiImage(Request $request)
	// {
	// 	$rules = [
		    
	// 	    'image.*' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
	// 	];

	// 	$validator = Validator::make($request->all(), $rules);
	// 	if ($validator->fails())
	// 	{
	// 	  $errorString = implode(",",$validator->messages()->all());
	// 	  return $this->sendError($errorString,'');
	// 	} 
	// 	else 
	// 	{
	// 		$user = $request->userData;
	// 		if (empty($user)) {
	// 			return $this->sendSpeError();
	// 		}
	// 		$data = array();
	// 		if($request->hasfile('image'))
	//          {
	//             foreach($request->file('image') as $file)
	//             {
	//                 $name = uniqid().'.'.$file->extension();
	//                 $file->move('public/images/user/', $name);  
	//                 $data[] = $name;  
	//             }
	//          }
	         
	//          $postArray['image'] = json_encode($data);
	//          $login = User::where('id',$user->id)->update($postArray);
	//          return $this->sendResponse('Your profile updated successfully.');
	// 	}
	// }

}