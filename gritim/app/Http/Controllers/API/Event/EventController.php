<?php

namespace App\Http\Controllers\API\Event;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Controllers\API\CommonModal;
use App\Models\Event;
use App\User;
use App\Models\EventJoin;
use Validator;
use File;

class EventController extends BaseController
{
  public function listEvent(Request $request)
  {
    $rules = [
      'user_id' => 'required',
      'api_token' => 'required',
      'page' => 'required|numeric'
    ];

    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
      $errorString = implode(",", $validator->messages()->all());
      return $this->sendError($errorString, '');
    } else {
      $user = $request->userData;
      if (empty($user)) {
        return $this->sendSpeError();
      }

      $event_list = CommonModal::event_list($request->page, $user->id);

      if ($event_list) {

        $response = array(
          'user_id' => $user->id,
          'api_token' => $user->api_token,
          'event_list' => $event_list,
          'page' => $request->page + 1,
        );
        return $this->sendResponse('Event list loaded successfully.', $response);
      } else {
        $response = array(
          'user_id' => $user->id,
          'api_token' => $user->api_token,
          'page' => $request->page + 1,
        );
        return $this->sendResponse('Event list not found!', $response);
      }
    }
  }

  public function detailEvent(Request $request)
  {
    $rules = [
      'user_id' => 'required',
      'api_token' => 'required',
      'event_id' => 'required'
    ];

    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
      $errorString = implode(",", $validator->messages()->all());
      return $this->sendError($errorString, '');
    } else {
      $user = $request->userData;
      if (empty($user)) {
        return $this->sendSpeError();
      }

      $event_detail = CommonModal::event_detail($request->event_id, $user->id);

      if ($event_detail) {

        $response = array(
          'user_id' => $user->id,
          'api_token' => $user->api_token,
          'event_detail' => $event_detail,
        );
        return $this->sendResponse('Event Details loaded successfully.', $response);
      } else {
        $response = array(
          'user_id' => $user->id,
          'api_token' => $user->api_token,
          'event_id' => $request->event_id,
        );
        return $this->sendResponse('Event Detail not found!', $response);
      }
    }
  }

  public function joinLeftEvent(Request $request)
  {
    $rules = [
      'user_id' => 'required',
      'api_token' => 'required',
      'event_id' => 'required',
      'is_join' => 'required'
    ];

    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
      $errorString = implode(",", $validator->messages()->all());
      return $this->sendError($errorString, '');
    } else {
      $user = $request->userData;
      if (empty($user)) {
        return $this->sendSpeError();
      } else {
        $checkEventJoin = EventJoin::where(['user_id' => $request->user_id, 'event_id' => $request->event_id])->first();

        if (empty($checkEventJoin)) {
          try {
            $postArray = [
              'user_id' => $request->user_id,
              'event_id' => $request->event_id,
              'status' => $request->is_join
            ];

            $userEventJoin = EventJoin::create($postArray);
            $user_id = $userEventJoin->user_id;

            if ($user_id) {
              $user_details = CommonModal::user_details($user_id);
              $response = array(
                'join_id' => $userEventJoin->id,
                'api_token' => $user->api_token,
                'event_id' => $userEventJoin->event_id,
                'event_join_status' => $userEventJoin->status,
                'user_data' => $user_details
              );
              return $this->sendResponse('Event Join successfully.', $response);
            } else {
              return $this->sendError('Event Join Failed, please try again.');
            }
          } catch (Exception $e) {
            return $this->sendSpeError();
          }
        } else {
          $eventStatus = $checkEventJoin->status;
          if ($eventStatus == $request->is_join) {
            $user_id = $checkEventJoin->user_id;
            $user_details = CommonModal::user_details($user_id);

            $response = array(
              'join_id' => $checkEventJoin->id,
              'api_token' => $request->api_token,
              'event_id' => $request->event_id,
              'event_join_status' => $request->is_join,
              'user_data' => $user_details
            );
            if ($eventStatus == 0) {
              return $this->sendResponse('Already you canceled this event.', $response);
            } else {
              return $this->sendResponse('Already you join this event.', $response);
            }
          } else {
            try {
              $postArray = [
                'status' => $request->is_join
              ];

              $userEventUpdate = EventJoin::where(['user_id' => $request->user_id, 'event_id' => $request->event_id])->update($postArray);
              $user_id = $checkEventJoin->user_id;

              if ($userEventUpdate) {
                $user_details = CommonModal::user_details($user_id);
                $response = array(
                  'join_id' => $checkEventJoin->id,
                  'api_token' => $request->api_token,
                  'event_id' => $request->event_id,
                  'event_join_status' => $request->is_join,
                  'user_data' => $user_details
                );
                if ($request->is_join == 0) {

                  return $this->sendResponse('Event cancel successfully.', $response);
                } else {

                  return $this->sendResponse('Event Join successfully.', $response);
                }
              } else {
                return $this->sendError('Event Join Failed, please try again.');
              }
            } catch (Exception $e) {
              return $this->sendSpeError();
            }
          }
        }
      }
    }
  }

  public function ratingEvent(Request $request)
  {
    $rules = [
      'user_id' => 'required',
      'api_token' => 'required',
      'event_id' => 'required',
      'rating' => 'required'
    ];

    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
      $errorString = implode(",", $validator->messages()->all());
      return $this->sendError($errorString, '');
    } else {
      $user = $request->userData;
      if (empty($user)) {
        return $this->sendSpeError();
      }

      $checkEvent = Event::where(['id' => $request->event_id])->first();

      if ($checkEvent) {
        $rating = $checkEvent->event_rating;
        if($rating == '0'){
          $averageRating = $request->rating;
        }
        else{
          $sum = ($rating + $request->rating);
          $averageRating1 = $sum/2;
          $averageRating = number_format((float)$averageRating1, 2, '.', '');
        }
        
        try {
          $postArray = [
            'event_rating' => $averageRating
          ];

          $EventUpdate = Event::where(['id' => $request->event_id])->update($postArray);
          $user_details = CommonModal::user_details($request->user_id);
          if($EventUpdate){
            $response = array(
              'api_token' => $request->api_token,
              'event_id' => $request->event_id,
              'rating' => $averageRating,
              'user_data' => $user_details
            );
            return $this->sendResponse('Rating submitted successfully.', $response);
          }
          else{
            return $this->sendError('Rating does not submitted , please try again.');
          }
        } catch (Exception $e) {
          return $this->sendSpeError();
        }
        
      } else {
        $user_details = CommonModal::user_details($request->user_id);
        $response = array(
          'api_token' => $request->api_token,
          'event_id' => $request->event_id,
          'user_data' => $user_details
        );
        return $this->sendResponse('Event does not found', $response);
      }
    }
  }
}
