<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('event_title');
            $table->dateTime('start_time')->nullable();
            $table->dateTime('end_time')->nullable();
            $table->string('location')->nullable();
            $table->text('description')->nullable();
            $table->string('media')->nullable();
            $table->decimal('event_rating',8, 2)->nullable();
            $table->integer('is_free')->comment('0 = No, 1 = Yes')->nullable();
            $table->decimal('price',8, 2)->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('total_join')->nullable();
            $table->integer('total_left')->nullable();
            $table->integer('join_limit')->nullable();
            $table->integer('status')->nullable();
            $table->ipAddress('ip_address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
