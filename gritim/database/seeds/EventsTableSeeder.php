<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->insert(array(
        	array(
				'event_title' => 'Soccer Play',
				'start_time' => '2020-09-10 09:45:00',
				'end_time' => '2020-10-05 09:45:00',
				'location' => 'San Francisco, CA',
				'description' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit.',
				'media' => 'fef40628a173baeb6f4c05d65f52658b.jpg',
				'event_rating' => '4.5',
				'is_free' => '0',
				'price' => '9.99',
				'user_id' => '1',
				'total_join' => '3',
				'total_left' => '9',
				'join_limit' => '20',
				'status' => '1',
				'ip_address' => '::1',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
        	)
        ));
    }
}
