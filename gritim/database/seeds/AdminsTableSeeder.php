<?php
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('admins')->insert(array(
        	array(
				'name' => "admin",
				'email' => 'admin@gmail.com',
				'password' => bcrypt('123456789'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
        	),
        	array(
				'name' => "Mayur",
				'email' => 'mayur.silverfox121@gmail.com',
				'password' => bcrypt('123456789'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
        	)
        ));

    }
}