<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// clear application cache, clear route, clear view compiled files, clear config files
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    Artisan::call('config:clear');
    return "Application cache flushed";
});

Route::get('/', function () {
    return redirect(app()->getLocale());
});

Route::group([ 'prefix' => '{locale}', 'where' => ['locale' => '[a-zA-Z]{2}'], 
  'middleware' => 'setlocale' ], function() {

    Route::get('/', function () {
        return view('welcome');
    });

    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');
    
});

Route::group([ 'prefix' => '{locale}', 'where' => ['locale' => '[a-zA-Z]{2}'], 'middleware' => 'setlocale' ], function() {

  	// Admin Route
  	Route::namespace("Admin")->prefix('admin')->group(function(){
  		Route::get('/', 'HomeController@index')->name('admin.home');
  		
      // Admin Auth route
  		Route::namespace('Auth')->group(function(){
  			Route::get('/login', 'LoginController@showLoginForm')->name('admin.login');
  			Route::post('/login', 'LoginController@login');
  			Route::post('logout', 'LoginController@logout')->name('admin.logout');
  			
        Route::get('/password/confirm', 'ConfirmPasswordController@showConfirmForm')->name('password.confirm');
        Route::post('/password/confirm', 'ConfirmPasswordController@confirm');
        Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::get('/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('/password/reset', 'ResetPasswordController@reset')->name('password.update');
        Route::get('/password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
  		});

      // Admin profile route
      Route::namespace('Profile')->group(function(){
        Route::get('/profile', 'ProfileController@index')->name('profile');
        Route::post('/profile/changePassword', 'ProfileController@changePassword')->name('profile.changePassword');
        Route::post('/profile/updateProfile', 'ProfileController@updateProfile')->name('profile.updateProfile');
      });

      // user route
      Route::namespace('User')->group(function(){
        Route::get('/users', 'UserController@index')->name('users');
        Route::post('/users', 'UserController@getList')->name('users.list');
        Route::post('/users/status', 'UserController@actionStatus')->name('users.status');
        Route::post('/users/delete', 'UserController@destroy')->name('users.delete');
        Route::get('/users/add', 'UserController@create')->name('users.add');
        Route::post('/users/add', 'UserController@store')->name('users.actionAdd');
        Route::get('/users/edit/{id}', 'UserController@edit')->name('users.edit');
        Route::post('/users/edit', 'UserController@update')->name('users.actionEdit');
        Route::get('/users/show/{id}', 'UserController@show')->name('users.show');
      });

      Route::namespace('Event')->group(function(){
        Route::get('/events', 'EventController@index')->name('events');
        Route::post('/events', 'EventController@getList')->name('events.list');
        Route::post('/events/status', 'EventController@actionStatus')->name('events.status');
        Route::post('/events/delete', 'EventController@destroy')->name('events.delete');
        Route::get('/events/add', 'EventController@create')->name('events.add');
        Route::post('/events/add', 'EventController@store')->name('events.actionAdd');
        Route::get('/events/edit/{id}', 'EventController@edit')->name('events.edit');
        Route::post('/events/edit', 'EventController@update')->name('events.actionEdit');
        Route::get('/events/show/{id}', 'EventController@show')->name('events.show');

        // Event JOin table route
        Route::get('/eventJoins', 'EventJoinController@index')->name('eventJoins');
        Route::post('/eventJoins', 'EventJoinController@getList')->name('eventJoins.list');
        Route::post('/eventJoins/delete', 'EventJoinController@destroy')->name('eventJoins.delete');
      });

  	});
    
});
