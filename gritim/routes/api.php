<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//User api

Route::middleware(['basicAuth'])->prefix('v1')->namespace('API')->group(function(){

	// Route::post('/register','Auth\AuthController@register');
	Route::post('/login','Auth\AuthController@login');
	Route::post('/forgotPassword', 'Auth\AuthController@forgotPassword');
	Route::post('/verifyOtp', 'Auth\AuthController@verifyOtp');
	Route::post('/resetPassword', 'Auth\AuthController@resetPassword');
	// Route::post('/facebook_login','Project\SocialloginController@facebook');
	// Route::post('/google_login','Project\SocialloginController@google');
});

// Protected with APIToken Middleware User
Route::middleware(['basicAuth','APIToken'])->prefix('v1')->namespace('API')->group(function(){

 	Route::post('/logout','Auth\AuthController@logout');

 	// User route
 	Route::namespace('User')->group(function(){
 		Route::post('/changePassword','UserController@changePassword');
 		Route::post('/updateProfile','UserController@updateProfile');
 		// Route::post('/uploadMultiImage','UserController@uploadMultiImage');
 		Route::post('/listEvent','UserController@updateProfile');
 	});

 	Route::namespace('Event')->group(function(){
 		Route::post('/listEvent','EventController@listEvent');
		 Route::post('/joinLeftEvent','EventController@joinLeftEvent');
		 Route::post('/detailEvent','EventController@detailEvent');
		 Route::post('/ratingEvent','EventController@ratingEvent');
 	});


});